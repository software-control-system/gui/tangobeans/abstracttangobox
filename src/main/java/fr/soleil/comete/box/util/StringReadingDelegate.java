package fr.soleil.comete.box.util;

import fr.soleil.data.source.AbstractDataSource;

public class StringReadingDelegate implements SourceReadingDelegate<String> {

    @Override
    public String readData(AbstractDataSource<?> source) throws Exception {
        return String.valueOf(source.getData());
    }

}
