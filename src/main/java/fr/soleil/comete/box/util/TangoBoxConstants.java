package fr.soleil.comete.box.util;

import fr.soleil.lib.project.resource.MessageManager;
import fr.soleil.lib.project.swing.icons.Icons;

/**
 * Utility class that stores constants that can be used anywhere in AbstractTangoBox project.
 * 
 * @author GIRARDOT
 *
 */
public interface TangoBoxConstants {

    /**
     * Default {@link Icons} to use in AbstractTangoBox.
     */
    public static final Icons DEFAULT_ICONS = new Icons("fr.soleil.comete.box.icons");

    /**
     * Default {@link MessageManager} to use in AbstractTangoBox.
     */
    public static final MessageManager DEFAULT_MESSAGE_MANAGER = new MessageManager("fr.soleil.comete.box");

}
