package fr.soleil.comete.box.util;

import javax.swing.SwingConstants;

import fr.soleil.comete.swing.Label;
import fr.soleil.lib.project.ObjectUtils;

/**
 * A {@link Label} that is supposed to be associated with a device.
 * 
 * @author girardot
 */
public class DeviceLabel extends Label {

    private static final long serialVersionUID = 6826918218010591961L;

    private static final String NO_DEVICE = "No device";

    private String deviceName;
    private String text;

    public DeviceLabel() {
        super();
        setHorizontalAlignment(SwingConstants.CENTER);
        deviceName = null;
        super.setText(NO_DEVICE);
    }

    @Override
    public void setText(String text) {
        this.text = text;
        computeToolTipText();
    }

    /**
     * Returns this {@link DeviceLabel}'s device name
     * 
     * @return A {@link String}
     */
    public String getDeviceName() {
        return deviceName;
    }

    /**
     * Sets this {@link DeviceLabel}'s device name
     * 
     * @param deviceName The device name to set
     */
    public void setDeviceName(String deviceName) {
        String name = deviceName;
        if (name != null) {
            name = name.trim();
            if (name.isEmpty()) {
                name = null;
            }
        }
        if (!ObjectUtils.sameObject(name, this.deviceName)) {
            this.deviceName = name;
            if (name == null) {
                super.setText(NO_DEVICE);
            }
            else {
                super.setText(name);
            }
            computeToolTipText();
        }
    }

    /**
     * Computes this {@link DeviceLabel}'s tooltip text
     */
    protected void computeToolTipText() {
        StringBuilder tooltipBuilder;
        String oldText = text;
        String device = deviceName;
        if (oldText != null) {
            oldText = oldText.trim();
            if (oldText.isEmpty()) {
                oldText = null;
            }
        }
        if (device == null) {
            if (oldText == null) {
                tooltipBuilder = null;
            }
            else {
                tooltipBuilder = createToolTipBuilder();
                tooltipBuilder.append(oldText);
            }
        }
        else {
            tooltipBuilder = createToolTipBuilder();
            tooltipBuilder.append(device);
            if (oldText != null) {
                tooltipBuilder.append(":<p>").append(oldText).append("</p>");
            }
        }
        if (tooltipBuilder == null) {
            setToolTipText(null);
        }
        else {
            finalizeTooltipBuilder(tooltipBuilder);
            setToolTipText(tooltipBuilder.toString());
        }
    }

    /**
     * Creates a new {@link StringBuilder} with html prepared text
     * 
     * @return A {@link StringBuilder}
     */
    protected StringBuilder createToolTipBuilder() {
        return new StringBuilder("<html><body>");
    }

    /**
     * Appends the html closing tags to a {@link StringBuilder}
     * 
     * @param builder The {@link StringBuilder}
     */
    protected void finalizeTooltipBuilder(StringBuilder builder) {
        builder.append("</body></html>");
    }

}
