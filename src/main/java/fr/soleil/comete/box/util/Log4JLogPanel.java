package fr.soleil.comete.box.util;

import java.awt.BorderLayout;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

import javax.swing.Box;
import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFileChooser;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextField;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import javax.swing.filechooser.FileFilter;

import org.slf4j.LoggerFactory;

import fr.soleil.comete.box.util.Log4JLogViewer.Level;
import fr.soleil.comete.box.util.renderer.LevelListCellRenderer;
import fr.soleil.comete.swing.util.CometeUtils;
import fr.soleil.data.mediator.Mediator;
import fr.soleil.lib.project.application.logging.LogViewer;
import fr.soleil.lib.project.file.FileUtils;
import fr.soleil.lib.project.swing.ConstrainedCheckBox;
import fr.soleil.lib.project.swing.file.ExtensionFileFilter;
import fr.soleil.lib.project.swing.icons.Icons;

/**
 * A {@link JPanel} dedicated in log4j logs treatments
 * 
 * @author GIRARDOT
 * @deprecated Use {@link LogViewer} or {@link DeviceLogViewer} instead
 */
@Deprecated
public class Log4JLogPanel extends JPanel implements ActionListener, ItemListener, DocumentListener {

    private static final long serialVersionUID = 1010091172381666822L;

    protected static final Icons DEFAULT_ICONS = new Icons("fr.soleil.comete.box.icons");
    // save log icon
    protected static final ImageIcon SAVE_ICON = DEFAULT_ICONS.getIcon("tangobox.log.save");
    // save log as icon
    protected static final ImageIcon SAVE_AS_ICON = DEFAULT_ICONS.getIcon("tangobox.log.saveAs");
    protected static final Insets START_MARGIN = new Insets(5, 5, 5, 5);
    protected static final Insets DEFAULT_MARGIN = new Insets(5, 0, 5, 5);
    protected static final Insets START_REDUCED_MARGIN = new Insets(5, 5, 5, 0);
    protected static final Insets REDUCED_MARGIN = new Insets(5, 0, 5, 0);
    protected static final String HTML = "html";
    protected static final String TXT = "txt";
    // os way to end line in files
    protected static final String CRLF = System.getProperty("line.separator");

    protected final Log4JLogViewer logViewer;
    protected final JScrollPane logViewerScrollPane;
    protected final JComboBox<Level> levelFilteringComboBox;
    protected final LevelListCellRenderer renderer;
    protected final ConstrainedCheckBox preciseLevelBox;
    protected final ConstrainedCheckBox reversedBox;
    protected final JTextField textFilteringField;
    protected final ConstrainedCheckBox textFilteringCaseSentiveBox;
    protected final JPanel levelFilteringPanel;
    protected final JLabel levelFilteringTitle;
    protected final JLabel textFilteringTitle;
    protected final JFileChooser fileChooser;
    protected final JButton saveButton, saveAsButton;

    public Log4JLogPanel(Log4JLogViewer logViewer) {
        this(logViewer, true);
    }

    public Log4JLogPanel(Log4JLogViewer logViewer, boolean layout) {
        super(new BorderLayout());
        this.logViewer = logViewer;
        logViewerScrollPane = new JScrollPane(logViewer);
        renderer = new LevelListCellRenderer();
        levelFilteringComboBox = new JComboBox<>();
        levelFilteringComboBox.addItem(Level.ERROR);
        levelFilteringComboBox.addItem(Level.WARN);
        levelFilteringComboBox.addItem(Level.INFO);
        levelFilteringComboBox.addItem(Level.DEBUG);
        levelFilteringComboBox.addItem(Level.ALL);
        levelFilteringComboBox.addItemListener(this);
        levelFilteringComboBox.setSelectedItem(Level.INFO);
        levelFilteringComboBox.setRenderer(renderer);
        preciseLevelBox = new ConstrainedCheckBox("Set as minimum log level");
        preciseLevelBox.setSelected(!logViewer.isPreciseLevelFiltering());
        preciseLevelBox.addActionListener(this);
        reversedBox = new ConstrainedCheckBox("Most recent logs first");
        reversedBox.setSelected(logViewer.isReversed());
        reversedBox.addActionListener(this);
        textFilteringCaseSentiveBox = new ConstrainedCheckBox("Case sensitive");
        textFilteringCaseSentiveBox.setSelected(logViewer.isFilteredTextCaseSensitive());
        textFilteringCaseSentiveBox.addActionListener(this);
        textFilteringField = new JTextField(5);
        textFilteringField.setMinimumSize(textFilteringField.getPreferredSize());
        textFilteringField.setText(logViewer.getFilteredText());
        textFilteringField.getDocument().addDocumentListener(this);
        levelFilteringTitle = new JLabel("Filtered log level");
        textFilteringTitle = new JLabel("Show only log containing");
        saveButton = new JButton("Save", SAVE_ICON);
        saveButton.setMargin(CometeUtils.getzInset());
        saveButton.addActionListener(this);
        saveAsButton = new JButton("Save as", SAVE_AS_ICON);
        saveAsButton.setMargin(CometeUtils.getzInset());
        saveAsButton.addActionListener(this);
        fileChooser = new JFileChooser(System.getProperty("user.home"));
        fileChooser.addChoosableFileFilter(new ExtensionFileFilter(HTML));
        fileChooser.addChoosableFileFilter(new ExtensionFileFilter(TXT));

        levelFilteringPanel = new JPanel(new GridBagLayout());
        if (layout) {
            layoutFilteringPanel();
        }
        add(logViewerScrollPane, BorderLayout.CENTER);
        add(levelFilteringPanel, BorderLayout.SOUTH);

        addIcon(Level.DEBUG, DEFAULT_ICONS.getIcon("tangobox.log.debug"));
        addIcon(Level.INFO, DEFAULT_ICONS.getIcon("tangobox.log.info"));
        addIcon(Level.WARN, DEFAULT_ICONS.getIcon("tangobox.log.warn"));
        addIcon(Level.ERROR, DEFAULT_ICONS.getIcon("tangobox.log.error"));
    }

    protected void layoutFilteringPanel() {
        int x = 0;
        GridBagConstraints levelFilteringTitleConstraints = new GridBagConstraints();
        levelFilteringTitleConstraints.fill = GridBagConstraints.VERTICAL;
        levelFilteringTitleConstraints.gridx = x++;
        levelFilteringTitleConstraints.gridy = 0;
        levelFilteringTitleConstraints.weightx = 0;
        levelFilteringTitleConstraints.weighty = 0;
        levelFilteringTitleConstraints.insets = START_MARGIN;
        levelFilteringPanel.add(levelFilteringTitle, levelFilteringTitleConstraints);
        GridBagConstraints levelFilteringComboBoxConstraints = new GridBagConstraints();
        levelFilteringComboBoxConstraints.fill = GridBagConstraints.VERTICAL;
        levelFilteringComboBoxConstraints.gridx = x++;
        levelFilteringComboBoxConstraints.gridy = 0;
        levelFilteringComboBoxConstraints.weightx = 0;
        levelFilteringComboBoxConstraints.weighty = 0;
        levelFilteringComboBoxConstraints.insets = DEFAULT_MARGIN;
        levelFilteringPanel.add(levelFilteringComboBox, levelFilteringComboBoxConstraints);
        GridBagConstraints preciseLevelBoxConstraints = new GridBagConstraints();
        preciseLevelBoxConstraints.fill = GridBagConstraints.VERTICAL;
        preciseLevelBoxConstraints.gridx = x++;
        preciseLevelBoxConstraints.gridy = 0;
        preciseLevelBoxConstraints.weightx = 0;
        preciseLevelBoxConstraints.weighty = 0;
        preciseLevelBoxConstraints.insets = DEFAULT_MARGIN;
        levelFilteringPanel.add(preciseLevelBox, preciseLevelBoxConstraints);
        GridBagConstraints reversedBoxConstraints = new GridBagConstraints();
        reversedBoxConstraints.fill = GridBagConstraints.VERTICAL;
        reversedBoxConstraints.gridx = x++;
        reversedBoxConstraints.gridy = 0;
        reversedBoxConstraints.weightx = 0;
        reversedBoxConstraints.weighty = 0;
        reversedBoxConstraints.insets = DEFAULT_MARGIN;
        levelFilteringPanel.add(reversedBox, reversedBoxConstraints);
        GridBagConstraints textFilteringTitleConstraints = new GridBagConstraints();
        textFilteringTitleConstraints.fill = GridBagConstraints.VERTICAL;
        textFilteringTitleConstraints.gridx = x++;
        textFilteringTitleConstraints.gridy = 0;
        textFilteringTitleConstraints.weightx = 0;
        textFilteringTitleConstraints.weighty = 0;
        textFilteringTitleConstraints.insets = START_REDUCED_MARGIN;
        levelFilteringPanel.add(textFilteringTitle, textFilteringTitleConstraints);
        GridBagConstraints textFilteringFieldConstraints = new GridBagConstraints();
        textFilteringFieldConstraints.fill = GridBagConstraints.VERTICAL;
        textFilteringFieldConstraints.gridx = x++;
        textFilteringFieldConstraints.gridy = 0;
        textFilteringFieldConstraints.weightx = 0;
        textFilteringFieldConstraints.weighty = 0;
        textFilteringFieldConstraints.insets = REDUCED_MARGIN;
        levelFilteringPanel.add(textFilteringField, textFilteringFieldConstraints);
        GridBagConstraints textFilteringCaseSentiveBoxConstraints = new GridBagConstraints();
        textFilteringCaseSentiveBoxConstraints.fill = GridBagConstraints.VERTICAL;
        textFilteringCaseSentiveBoxConstraints.gridx = x++;
        textFilteringCaseSentiveBoxConstraints.gridy = 0;
        textFilteringCaseSentiveBoxConstraints.weightx = 0;
        textFilteringCaseSentiveBoxConstraints.weighty = 0;
        textFilteringCaseSentiveBoxConstraints.insets = DEFAULT_MARGIN;
        levelFilteringPanel.add(textFilteringCaseSentiveBox, textFilteringCaseSentiveBoxConstraints);
        GridBagConstraints saveLogConstraints = new GridBagConstraints();
        saveLogConstraints.fill = GridBagConstraints.VERTICAL;
        saveLogConstraints.gridx = x++;
        saveLogConstraints.gridy = 0;
        saveLogConstraints.weightx = 0;
        saveLogConstraints.weighty = 1;
        saveLogConstraints.insets = START_MARGIN;
        levelFilteringPanel.add(saveButton, saveLogConstraints);
        GridBagConstraints saveLogAsConstraints = new GridBagConstraints();
        saveLogAsConstraints.fill = GridBagConstraints.VERTICAL;
        saveLogAsConstraints.gridx = x++;
        saveLogAsConstraints.gridy = 0;
        saveLogAsConstraints.weightx = 0;
        saveLogAsConstraints.weighty = 1;
        saveLogAsConstraints.insets = DEFAULT_MARGIN;
        levelFilteringPanel.add(saveAsButton, saveLogAsConstraints);
        GridBagConstraints glueConstraints = new GridBagConstraints();
        glueConstraints.fill = GridBagConstraints.VERTICAL;
        glueConstraints.gridx = x++;
        glueConstraints.gridy = 0;
        glueConstraints.weightx = 1;
        glueConstraints.weighty = 0;
        levelFilteringPanel.add(Box.createGlue(), glueConstraints);
    }

    public Log4JLogViewer getLogViewer() {
        return logViewer;
    }

    public void addIcon(Level level, Icon icon) {
        renderer.addIcon(level, icon);
        levelFilteringComboBox.revalidate();
        levelFilteringComboBox.repaint();
    }

    public void removeIcon(Level level) {
        renderer.removeIcon(level);
        levelFilteringComboBox.revalidate();
        levelFilteringComboBox.repaint();
    }

    public void clearIcons() {
        renderer.clearIcons();
        levelFilteringComboBox.revalidate();
        levelFilteringComboBox.repaint();
    }

    public void addLevel(Level level) {
        levelFilteringComboBox.addItem(level);
    }

    public void removeLevel(Level level) {
        levelFilteringComboBox.removeItem(level);
    }

    public void clearLevels() {
        levelFilteringComboBox.removeAllItems();
    }

    public Level getSelectedLevel() {
        return (Level) levelFilteringComboBox.getSelectedItem();
    }

    public void setSelectedLevel(Level level) {
        levelFilteringComboBox.setSelectedItem(level);
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        if (e != null) {
            if (e.getSource() == preciseLevelBox) {
                logViewer.setPreciseLevelFiltering(!preciseLevelBox.isSelected());
            } else if (e.getSource() == reversedBox) {
                logViewer.setReversed(reversedBox.isSelected());
            } else if (e.getSource() == textFilteringCaseSentiveBox) {
                logViewer.setFilteredTextCaseSensitive(textFilteringCaseSentiveBox.isSelected());
            } else if (e.getSource() == saveAsButton) {
                saveAs();
            } else if (e.getSource() == saveButton) {
                save(true);
            }
        }
    }

    @Override
    public void itemStateChanged(ItemEvent e) {
        if ((e != null) && (e.getSource() == levelFilteringComboBox) && (e.getStateChange() == ItemEvent.SELECTED)) {
            Object item = e.getItem();
            Level level;
            if (item instanceof Level) {
                level = (Level) item;
            } else {
                level = null;
            }
            logViewer.setFilteredLevel(level);
        }
    }

    @Override
    public void insertUpdate(DocumentEvent e) {
        updateFilter(e);
    }

    @Override
    public void removeUpdate(DocumentEvent e) {
        updateFilter(e);
    }

    @Override
    public void changedUpdate(DocumentEvent e) {
        updateFilter(e);
    }

    protected void updateFilter(DocumentEvent e) {
        if ((e != null) && (e.getDocument() == textFilteringField.getDocument())) {
            logViewer.setFilteredText(textFilteringField.getText());
        }
    }

    /**
     * Ask user where to save logs and then save them
     */
    protected void saveAs() {
        if (fileChooser.showSaveDialog(this) == JFileChooser.APPROVE_OPTION) {
            File toSave = fileChooser.getSelectedFile();
            FileFilter filter = fileChooser.getFileFilter();
            if (filter instanceof ExtensionFileFilter) {
                ExtensionFileFilter extensionFilter = (ExtensionFileFilter) filter;
                if (!extensionFilter.getExtension().equalsIgnoreCase(FileUtils.getExtension(toSave))) {
                    fileChooser
                            .setSelectedFile(new File(toSave.getAbsoluteFile() + "." + extensionFilter.getExtension()));
                }
            }
            save(false);
        }
    }

    /**
     * Saves logs
     * 
     * @param overWrite Whether to always overwrite when file already exists
     */
    protected void save(boolean overWrite) {
        final File toSave = fileChooser.getSelectedFile();
        if (toSave == null) {
            saveAs();
        } else {
            int choice = JOptionPane.YES_OPTION;
            if ((!overWrite) && toSave.exists()) {
                choice = JOptionPane.showConfirmDialog(this, toSave.getName() + " exists. Overwrite?", "Warning",
                        JOptionPane.YES_NO_OPTION);
            }
            if (choice == JOptionPane.YES_OPTION) {
                // save log in a separated thread
                new Thread("Save device log in " + toSave.getAbsolutePath()) {
                    @Override
                    public void run() {
                        String extension = FileUtils.getExtension(toSave);
                        String log;
                        if (HTML.equalsIgnoreCase(extension)) {
                            log = logViewer.getText();
                        } else {
                            StringBuilder builder = new StringBuilder();
                            String[] lines = logViewer.getFlatStringMatrix();
                            if (lines != null) {
                                for (String line : lines) {
                                    if ((line != null) && (!line.isEmpty())) {
                                        builder.append(line);
                                    }
                                    builder.append(CRLF);
                                }
                                if (builder.length() > 0) {
                                    // remove last CRLF
                                    builder.delete(builder.length() - CRLF.length(), builder.length());
                                }
                            }
                            log = builder.toString();
                        }
                        FileWriter writer = null;
                        try {
                            writer = new FileWriter(toSave);
                            writer.write(log);
                        } catch (IOException e) {
                            LoggerFactory.getLogger(Mediator.LOGGER_ACCESS)
                                    .error("Failed to write " + toSave.getAbsolutePath(), e);
                        } finally {
                            if (writer != null) {
                                try {
                                    writer.close();
                                } catch (IOException e) {
                                    LoggerFactory.getLogger(Mediator.LOGGER_ACCESS)
                                            .error("Failed to close " + toSave.getAbsolutePath(), e);
                                }
                            }
                        }
                    }
                }.start();
            }
        }
    }
}
