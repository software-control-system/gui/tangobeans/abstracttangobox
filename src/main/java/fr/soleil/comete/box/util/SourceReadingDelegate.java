package fr.soleil.comete.box.util;

import fr.soleil.data.source.AbstractDataSource;

public interface SourceReadingDelegate<T> {

    /**
     * Extracts some data from an {@link AbstractDataSource}.
     * 
     * @param source The {@link AbstractDataSource}.
     * @return The extracted data.
     * @throws Exception If some problem occurred when trying to access data
     * @see AbstractDataSource#getData()
     */
    public T readData(AbstractDataSource<?> source) throws Exception;

}
