package fr.soleil.comete.box.util;

import javax.swing.SwingConstants;

import fr.soleil.comete.swing.StringButton;
import fr.soleil.lib.project.ObjectUtils;
import fr.soleil.lib.project.application.logging.LogViewer;
import fr.soleil.lib.project.resource.MessageManager;
import fr.soleil.lib.project.swing.icons.DecorableIcon;
import fr.soleil.lib.project.swing.icons.Icons;

/**
 * A {@link LogViewer} that is dedicated to device logs and contains a {@link StringButton} that can be connected to
 * "ClearLogs" command.
 * <p>
 * You should use such a {@link DeviceLogViewer} only if you need that {@link StringButton}. Otherwise, you may use a
 * {@link LogViewer}.
 * </p>
 * <p>
 * In all cases, you will need either a {@link fr.soleil.comete.box.target.DeviceLogSpectrumAdapter} or a
 * {@link fr.soleil.comete.box.target.DeviceLogMatrixAdapter} to listen
 * to your device logs (depending on whether your device log attribute is a String spectrum or a String matrix
 * attribute).
 * </p>
 * <table border=1 style="border-collapse: collapse;">
 * <tr>
 * <th style="text-align: left;">Use case example:</th>
 * </tr>
 * <tr>
 * <td>
 * 
 * <pre>
 * StringScalarBox scalarBox = new StringScalarBox();
 * StringMatrixBox matrixBox = new StringMatrixBox();
 * DeviceLogViewer viewer = new DeviceLogViewer(applicationId);
 * // connect to "clear logs" command
 * TangoKey clearKey = generateClearDeviceLogsKey);
 * scalarBox.setUseErrorText(viewer.getClearDeviceLogsButton(), false);
 * scalarBox.connectWidget(viewer.getClearDeviceLogsButton(), clearKey);
 * // connect to "log" attribute
 * TangoKey logKey = generateLogAttributeKey();
 * DeviceLogSpectrumAdapter logAdapter = new DeviceLogSpectrumAdapter(viewer);
 * matrixBox.connectWidget(logAdapter, logKey);
 * </pre>
 * 
 * </td>
 * </tr>
 * </table>
 * 
 * @author GIRARDOT
 */
public class DeviceLogViewer extends LogViewer {

    private static final long serialVersionUID = -1093771214297139407L;

    protected static final DecorableIcon CLEAR_DEVICE_LOGS_ICON;
    static {
        CLEAR_DEVICE_LOGS_ICON = TangoBoxConstants.DEFAULT_ICONS.getDecorableIcon("tangobox.log.clear",
                "tangobox.device.decoration");
        CLEAR_DEVICE_LOGS_ICON.setDecorationHorizontalAlignment(SwingConstants.LEFT);
        CLEAR_DEVICE_LOGS_ICON.setDecorationVerticalAlignment(SwingConstants.BOTTOM);
        CLEAR_DEVICE_LOGS_ICON.setDecorated(true);
    }

    protected StringButton clearDeviceLogsButton;

    public DeviceLogViewer(String applicationIdForErrorFeedback) {
        super(applicationIdForErrorFeedback);
    }

    public DeviceLogViewer(String applicationIdForErrorFeedback, MessageManager messageManager, Icons icons) {
        super(applicationIdForErrorFeedback, messageManager, icons);
    }

    @Override
    protected void buildComponents() {
        clearDeviceLogsButton = new StringButton();
        clearDeviceLogsButton.setText(ObjectUtils.EMPTY_STRING);
        clearDeviceLogsButton.setIcon(CLEAR_DEVICE_LOGS_ICON);
        clearDeviceLogsButton
                .setToolTipText(TangoBoxConstants.DEFAULT_MESSAGE_MANAGER.getMessage("tangobox.log.device.clear"));
        clearDeviceLogsButton.setMargin(NO_MARGIN);
        super.buildComponents();
    }

    @Override
    protected void addButtons() {
        managementPanel.add(clearButton);
        managementPanel.add(clearDeviceLogsButton);
        managementPanel.add(saveButton);
        managementPanel.add(copyButton);
        managementPanel.add(playPausePanel);
        managementPanel.add(optionsButton);
    }

    /**
     * Returns whether the default "clear logs" button (not the "clear device logs" one) is visible.
     * 
     * @return A <code>boolean</code>.
     */
    public boolean isDefaultClearLogsButtonVisible() {
        return clearButton.isVisible();
    }

    /**
     * Sets visible or invisible the default "clear logs" button (not the "clear device logs" one).
     * 
     * @param visible Whether the button should be visible.
     */
    public void setDefaultClearLogsButtonVisible(boolean visible) {
        clearButton.setVisible(visible);
        repaint();
    }

    /**
     * Returns the {@link StringButton} that can be connected to the "ClearLogs" command.
     * 
     * @return A {@link StringButton}.
     */
    public StringButton getClearDeviceLogsButton() {
        return clearDeviceLogsButton;
    }

}
