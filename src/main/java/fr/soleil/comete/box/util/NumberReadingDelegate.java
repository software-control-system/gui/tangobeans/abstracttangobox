package fr.soleil.comete.box.util;

import fr.soleil.data.source.AbstractDataSource;
import fr.soleil.lib.project.ObjectUtils;
import fr.soleil.lib.project.math.UnsignedConverter;

public class NumberReadingDelegate implements SourceReadingDelegate<Number> {

    @Override
    public Number readData(AbstractDataSource<?> source) throws Exception {
        Number result = null;
        if (ObjectUtils.isNumberClass(source.getDataType().getConcernedClass())) {
            Number tmp = (Number) source.getData();
            result = source.isUnsigned() ? (Number) UnsignedConverter.convertUnsigned(tmp) : tmp;
        }
        return result;
    }

}
