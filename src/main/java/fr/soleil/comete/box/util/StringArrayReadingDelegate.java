package fr.soleil.comete.box.util;

import fr.soleil.data.container.matrix.AbstractMatrix;
import fr.soleil.data.source.AbstractDataSource;
import fr.soleil.lib.project.data.AFlatData;
import fr.soleil.lib.project.math.ArrayUtils;

public class StringArrayReadingDelegate implements SourceReadingDelegate<String[]> {

    @Override
    public String[] readData(AbstractDataSource<?> source) throws Exception {
        String[] result = null;
        Object data = source.getData();
        if (data instanceof String[]) {
            result = (String[]) data;
        } else if (data instanceof String[][]) {
            result = (String[]) ArrayUtils.convertArrayDimensionFromNTo1(data);
        } else if (data instanceof AFlatData<?>) {
            data = ((AFlatData<?>) data).getValue();
            if (data instanceof String[]) {
                result = (String[]) data;
            }
        } else if (data instanceof AbstractMatrix<?>) {
            data = ((AbstractMatrix<?>) data).getFlatValue();
            if (data instanceof String[]) {
                result = (String[]) data;
            }
        }
        return result;
    }

}
