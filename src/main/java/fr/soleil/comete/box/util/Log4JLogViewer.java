package fr.soleil.comete.box.util;

import java.awt.Color;
import java.awt.Font;

import fr.soleil.comete.definition.data.target.matrix.ITextMatrixComponent;
import fr.soleil.comete.swing.Label;
import fr.soleil.lib.project.ObjectUtils;
import fr.soleil.lib.project.application.logging.LogViewer;
import fr.soleil.lib.project.math.ArrayUtils;
import fr.soleil.lib.project.swing.LoggingDelegate;

/**
 * An abstract viewer that receives some logs and parses them to use the right html text format
 * 
 * @author GIRARDOT
 * @deprecated Log management is now done by {@link LogViewer}
 */
@Deprecated
public abstract class Log4JLogViewer extends Label implements ITextMatrixComponent {

    private static final long serialVersionUID = 8701705162047018053L;

    protected static final int OFF_INT = Integer.MAX_VALUE;
    protected static final int FATAL_INT = 50000;
    protected static final int ERROR_INT = 40000;
    protected static final int WARN_INT = 30000;
    protected static final int INFO_INT = 20000;
    protected static final int DEBUG_INT = 10000;
    protected static final int TRACE_INT = 5000;
    protected static final int ALL_INT = Integer.MIN_VALUE;

    protected static final String WARNING = "WARNING";
    protected static final Color WARNING_COLOR = new Color(238, 118, 33); // orange
    protected static final String DEFAULT_ERROR_TEXT = "<html><head></head><body><div style=\"color:red;\"><b><u>No Data</u></b></div></body></html>";

    protected static final String STYLE = " style=\"";
    protected static final String COLOR = "color:#";
    protected static final String FONT_SIZE = "font-size:";
    protected static final String POINTS = "pt;";
    protected static final String ATTRIBUTE_SEPARATOR = ";";
    protected static final String FONT_FAMILY = "font-family:";
    protected static final String FONT_WEIGHT_BOLD = "font-weight:bold;";
    protected static final String FONT_WEIGHT_NORMAL = "font-weight:normal;";
    protected static final String FONT_STYLE_ITALIC = "font-style:italic;";
    protected static final String FONT_STYLE_NORMAL = "font-style:normal;";
    protected static final String ANY_HTML_TAG = "<.*>";
    protected static final String EMPTY_PARAGRAPH = "<p></p>";
    protected static final String NEW_LINE = "\n";
    protected static final String NEW_LINE_HTML = "<br />";
    protected static final String NEW_LINE_HTML_START = "<br";
    protected static final String DIV_START = "<div";
    protected static final String DIV_END = "</div>";
    protected static final String PARAGRAPH_START = "<p>";
    protected static final String PARAGRAPH_END = "</p>";

    protected String errorAttribute;
    protected String warningAttribute;
    protected String infoAttribute;
    protected String debugAttribute;

    // the type of level to display
    protected Level filteredLevel;
    protected boolean preciseLevelFiltering;

    // filter received text
    protected String filteredText;
    protected boolean filteredTextCaseSensitive;

    protected String[] lastLines;
    private final boolean initialized;

    private final LoggingDelegate delegate;

    public Log4JLogViewer() {
        super();
        lastLines = null;
        filteredLevel = null;
        preciseLevelFiltering = false;
        buildDivAttributes();
        initialized = true;
        setBackground(Color.WHITE);
        setOpaque(true);
        delegate = new LoggingDelegate(this, true);
    }

    @Override
    public void setFont(Font font) {
        super.setFont(font);
        if (initialized) {
            buildDivAttributes();
            updateText();
        }
    }

    protected void buildDivAttributes() {
        errorAttribute = appendStyleToStringBuilder(new StringBuilder(), getColorForLevel(Level.ERROR), false, false, 1,
                null).toString();
        warningAttribute = appendStyleToStringBuilder(new StringBuilder(), getColorForLevel(Level.WARN), false, false,
                1, null).toString();
        infoAttribute = appendStyleToStringBuilder(new StringBuilder(), getColorForLevel(Level.INFO), false, false, 1,
                null).toString();
        debugAttribute = appendStyleToStringBuilder(new StringBuilder(), getColorForLevel(Level.DEBUG), false, false, 1,
                null).toString();
    }

    @Override
    public String[] getFlatStringMatrix() {
        return lastLines;
    }

    @Override
    public String[][] getStringMatrix() {
        // should not be used
        return (String[][]) ArrayUtils.convertArrayDimensionFrom1ToN(lastLines, getMatrixDataHeight(String.class),
                getMatrixDataWidth(String.class));
    }

    @Override
    public void setFlatStringMatrix(String[] value, int width, int height) {
        lastLines = value;
        updateText();
    }

    @Override
    public void setStringMatrix(String[][] value) {
        String[] lineValue;
        if (value == null) {
            lineValue = null;
        } else {
            lineValue = (String[]) ArrayUtils.convertArrayDimensionFromNTo1(value);
        }
        lastLines = lineValue;
        updateText();
    }

    @Override
    public int getMatrixDataHeight(Class<?> concernedDataClass) {
        return (lastLines == null ? 0 : 1);
    }

    @Override
    public int getMatrixDataWidth(Class<?> concernedDataClass) {
        return (lastLines == null ? 0 : lastLines.length);
    }

    @Override
    public boolean isPreferFlatValues(Class<?> concernedDataClass) {
        return true;
    }

    public boolean isReversed() {
        return delegate.isReversed();
    }

    public void setReversed(boolean reversed) {
        if (reversed != isReversed()) {
            delegate.setReversed(reversed);
            updateText();
        }
    }

    /**
     * Sets text with previously registered dao content
     */
    protected void updateText() {
        final String text;
        String[] lines = lastLines;
        if (lines == null) {
            text = DEFAULT_ERROR_TEXT;
        } else {
            StringBuilder buffer = new StringBuilder("<html><head></head><body>");
            if (delegate.isReversed()) {
                for (int i = lines.length - 1; i >= 0; i--) {
                    buffer = appendHtmlLineToBuffer(buffer, lines[i]);
                }
            } else {
                for (String line : lines) {
                    buffer = appendHtmlLineToBuffer(buffer, line);
                }
            }
            buffer.append("</body></html>");
            text = buffer.toString();
        }
        setText(text);
        delegate.updateScroll();
    }

    protected StringBuilder appendHtmlLineToBuffer(StringBuilder buffer, String line) {
        if (buffer == null) {
            buffer = new StringBuilder();
        }
        if ((line == null) || (line.trim().isEmpty())) {
            buffer.append(EMPTY_PARAGRAPH);
        } else {
            boolean caseSensitive = filteredTextCaseSensitive;
            String filter = getFilteredText(caseSensitive);
            line = line.trim().replace(NEW_LINE, NEW_LINE_HTML);
            Level level = parseLevel(line);
            String lineToWrite = line;
            if (isValidLevel(level)) {
                String divAttributes = getDivAttributes(level);
                int start = buffer.length();
                boolean added = false;
                int count = 0;
                String ref = lineToWrite.toLowerCase().trim();
                String tmp = ref;
                while (tmp.startsWith(NEW_LINE_HTML)) {
                    tmp = tmp.substring(NEW_LINE_HTML.length()).trim();
                    count++;
                }
                String[] separated = lineToWrite.split(NEW_LINE_HTML_START);
                if (divAttributes == null) {
                    if (ref.startsWith(NEW_LINE_HTML_START) && ref.startsWith(DIV_START) && ref.startsWith("<h")) {
                        if (lineMatchesFilter(lineToWrite, filter, caseSensitive)) {
                            // try to recognize HTML tags that already bring new lines
                            buffer.append(lineToWrite);
                            added = true;
                        }
                    } else {
                        buffer.append(PARAGRAPH_START);
                        while (count > 0) {
                            buffer.append(NEW_LINE_HTML);
                            count--;
                        }
                        for (String lineToAdd : separated) {
                            if (lineMatchesFilter(lineToAdd, filter, caseSensitive)) {
                                buffer.append(lineToAdd).append(NEW_LINE_HTML_START);
                                added = true;
                            }
                        }
                        if (added) {
                            // delete last <br/>
                            buffer.delete(buffer.length() - NEW_LINE_HTML_START.length(), buffer.length());
                            // close div
                            buffer.append(PARAGRAPH_END);
                        }
                    }
                    ref = null;
                } else {
                    buffer.append(DIV_START).append(divAttributes).append('>');
                    while (count > 0) {
                        buffer.append(NEW_LINE_HTML);
                        count--;
                    }
                    for (String lineToAdd : separated) {
                        if (lineMatchesFilter(lineToAdd, filter, caseSensitive)) {
                            buffer.append(lineToAdd).append(NEW_LINE_HTML_START);
                            added = true;
                        }
                    }
                    if (added) {
                        // delete last <br/>
                        buffer.delete(buffer.length() - NEW_LINE_HTML_START.length(), buffer.length());
                        // close div
                        buffer.append(DIV_END);
                    }
                }
                if ((!added) && (start != buffer.length())) {
                    // cancel text initializations
                    buffer.delete(start, buffer.length());
                }
            }
        }
        return buffer;
    }

    protected String getFilteredText(boolean caseSensitive) {
        String result = filteredText;
        if ((result != null) && (!caseSensitive)) {
            result = result.toLowerCase();
        }
        return result;
    }

    protected boolean lineMatchesFilter(String line, String filteredText, boolean caseSensitive) {
        boolean ok;
        if (line == null) {
            ok = false;
        } else if (filteredText == null) {
            ok = true;
        } else {
            if (!caseSensitive) {
                line = line.toLowerCase();
            }
            // get line without html tags
            line = line.replaceAll(ANY_HTML_TAG, ObjectUtils.EMPTY_STRING);
            ok = line.contains(filteredText);
        }
        return ok;
    }

    protected boolean isValidLevel(Level level) {
        boolean valid;
        if ((filteredLevel == null) || Level.ALL.equals(filteredLevel)) {
            valid = true;
        } else if (preciseLevelFiltering) {
            if (filteredLevel.equals(level)) {
                valid = true;
            } else {
                valid = false;
            }
        } else if (level == null) {
            valid = false;
        } else {
            valid = level.isGreaterOrEqual(filteredLevel);
        }
        return valid;
    }

    // Appends integer hexadecimal string representation to StringBuilder
    private static StringBuilder appendHexStringToStringBuilder(StringBuilder buffer, int i) {
        if (buffer == null) {
            buffer = new StringBuilder();
        }
        String hexString = Integer.toHexString(i).toUpperCase().trim();
        if (hexString.length() == 1) {
            buffer.append("0");
        }
        buffer.append(hexString);
        hexString = null;
        return buffer;
    }

    protected abstract Level parseLevel(String line);

    protected final Level stringLevelToLevel(String stringLevel) {
        Level level = Level.toLevel(stringLevel, null);
        if ((level == null) && WARNING.equals(stringLevel)) {
            level = Level.WARN;
        }
        return level;
    }

    protected String getDivAttributes(Level level) {
        String result = null;
        if (level != null) {
            if (Level.ERROR.equals(level)) {
                result = errorAttribute;
            } else if (Level.WARN.equals(level)) {
                result = warningAttribute;
            } else if (Level.INFO.equals(level)) {
                result = infoAttribute;
            } else if (Level.DEBUG.equals(level)) {
                result = debugAttribute;
            }
        }
        return result;
    }

    // returns the Color associated with a particular level
    private static Color getColorForLevel(Level level) {
        Color result = Color.BLACK;
        if (level != null) {
            if (Level.ERROR.equals(level)) {
                result = Color.RED;
            } else if (Level.WARN.equals(level)) {
                result = WARNING_COLOR;
            } else if (Level.INFO.equals(level)) {
                result = Color.BLUE;
            } else if (Level.DEBUG.equals(level)) {
                result = Color.GRAY;
            }
        }
        return result;
    }

    private StringBuilder appendStyleToStringBuilder(StringBuilder builder, Color color, boolean bold, boolean italic,
            double fontMult, String family) {
        if (builder == null) {
            builder = new StringBuilder();
        }
        builder.append(STYLE);
        if (color != null) {
            builder.append(COLOR);
            appendHexStringToStringBuilder(builder, color.getRed());
            appendHexStringToStringBuilder(builder, color.getGreen());
            appendHexStringToStringBuilder(builder, color.getBlue());
            builder.append(';');
        }
        Font font = getFont();
        if ((fontMult > 0) && (fontMult != 1)) {
            builder.append(FONT_SIZE).append(Long.toString(Math.round(fontMult * font.getSize()))).append(POINTS);
        } else {
            builder.append(FONT_SIZE).append(font.getSize()).append(POINTS);
        }
        builder.append(FONT_FAMILY);
        if ((family == null) || family.trim().isEmpty()) {
            builder.append(font.getFamily());
        } else {
            builder.append(family);
        }
        builder.append(ATTRIBUTE_SEPARATOR);
        if (bold) {
            builder.append(FONT_WEIGHT_BOLD);
        } else {
            builder.append(FONT_WEIGHT_NORMAL);
        }
        if (italic) {
            builder.append(FONT_STYLE_ITALIC);
        } else {
            builder.append(FONT_STYLE_NORMAL);
        }
        builder.append('"');
        return builder;
    }

    /**
     * Returns the filtered message level
     * 
     * @return A {@link Level}
     */
    public Level getFilteredLevel() {
        return filteredLevel;
    }

    /**
     * Sets the message level to filter
     * 
     * @param filteredLevel A {@link String}. <code>null</code> to have no filter
     */
    public void setFilteredLevel(Level filteredLevel) {
        this.filteredLevel = filteredLevel;
        updateText();
    }

    public boolean isPreciseLevelFiltering() {
        return preciseLevelFiltering;
    }

    public void setPreciseLevelFiltering(boolean preciseLevelFiltering) {
        this.preciseLevelFiltering = preciseLevelFiltering;
        updateText();
    }

    public String getFilteredText() {
        return filteredText == null ? ObjectUtils.EMPTY_STRING : filteredText;
    }

    public void setFilteredText(String filteredText) {
        String text;
        if (filteredText == null) {
            text = null;
        } else {
            text = filteredText.trim();
            if (text.isEmpty()) {
                text = null;
            }
        }
        if (!ObjectUtils.sameObject(this.filteredText, text)) {
            this.filteredText = text;
            updateText();
        }
    }

    public boolean isFilteredTextCaseSensitive() {
        return filteredTextCaseSensitive;
    }

    public void setFilteredTextCaseSensitive(boolean filteredTextCaseSensitive) {
        if (this.filteredTextCaseSensitive != filteredTextCaseSensitive) {
            this.filteredTextCaseSensitive = filteredTextCaseSensitive;
            updateText();
        }
    }

    // ///////////// //
    // Inner classes //
    // ///////////// //

    /**
     * An {@link Enum} that represents log4j levels
     * 
     * @author GIRARDOT
     */
    public static enum Level {
        OFF(OFF_INT), FATAL(FATAL_INT), ERROR(ERROR_INT), WARN(WARN_INT), INFO(INFO_INT), DEBUG(DEBUG_INT),
        TRACE(TRACE_INT), ALL(ALL_INT);

        private final int level;

        private Level(int level) {
            this.level = level;
        }

        /**
         * Returns <code>true</code> if this level has a higher or equal
         * level than the level passed as argument, <code>false</code> otherwise.
         * 
         * @return A <code>boolean</code>
         */
        public final boolean isGreaterOrEqual(Level r) {
            return level >= r.level;
        }

        /**
         * Returns the integer representation of this level.
         */
        public final int toInt() {
            return level;
        }

        /**
         * Convert the {@link String} passed as argument to a level. If the
         * conversion fails, then this method returns {@link #DEBUG}.
         * 
         * @param sArg The {@link String} to parse to {@link Level}.
         * @return A {@link Level}.
         */
        public static Level toLevel(String sArg) {
            return toLevel(sArg, DEBUG);
        }

        /**
         * Convert an integer passed as argument to a level. If the
         * conversion fails, then this method returns {@link #DEBUG}.
         * 
         * @param val The <code>int</code> to parse to {@link Level}.
         * @return A {@link Level}.
         */
        public static Level toLevel(int val) {
            return toLevel(val, DEBUG);
        }

        /**
         * Convert an integer passed as argument to a level. If the
         * conversion fails, then this method returns the specified default.
         * 
         * @param val The <code>int</code> to parse to {@link Level}.
         * @param defaultLevel The default {@link Level} to return.
         * @return A {@link Level}.
         */
        public static Level toLevel(int val, Level defaultLevel) {
            Level level;
            switch (val) {
                case ALL_INT:
                    level = ALL;
                    break;
                case DEBUG_INT:
                    level = Level.DEBUG;
                    break;
                case INFO_INT:
                    level = Level.INFO;
                    break;
                case WARN_INT:
                    level = Level.WARN;
                    break;
                case ERROR_INT:
                    level = Level.ERROR;
                    break;
                case FATAL_INT:
                    level = Level.FATAL;
                    break;
                case OFF_INT:
                    level = OFF;
                    break;
                case TRACE_INT:
                    level = Level.TRACE;
                    break;
                default:
                    level = defaultLevel;
                    break;
            }
            return level;
        }

        /**
         * Convert the string passed as argument to a level. If the
         * conversion fails, then this method returns the value of <code>defaultLevel</code>.
         * 
         * @param sArg The {@link String} to parse to {@link Level}.
         * @param defaultLevel The default {@link Level} to return.
         * @return A {@link Level}.
         */
        public static Level toLevel(String sArg, Level defaultLevel) {
            Level level = defaultLevel;
            if (sArg != null) {
                for (Level lvl : values()) {
                    if (sArg.equals(lvl.toString())) {
                        level = lvl;
                        break;
                    }
                }
            }
            return level;
        }

    }

}
