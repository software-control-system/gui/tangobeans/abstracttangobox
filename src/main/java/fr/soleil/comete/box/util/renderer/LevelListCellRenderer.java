package fr.soleil.comete.box.util.renderer;

import java.awt.Component;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import javax.swing.DefaultListCellRenderer;
import javax.swing.Icon;
import javax.swing.JLabel;
import javax.swing.JList;

import fr.soleil.comete.box.util.Log4JLogViewer.Level;

/**
 * A {@link DefaultListCellRenderer} dedicated in rendering {@link Level}
 * 
 * @author GIRARDOT
 * @deprecated Prefer using {@link fr.soleil.lib.project.swing.renderer.LevelListCellRenderer} instead.
 */
@Deprecated
public class LevelListCellRenderer extends DefaultListCellRenderer {

    private static final long serialVersionUID = -6288229739034127462L;

    private final Map<Level, Icon> icons;

    public LevelListCellRenderer() {
        super();
        icons = new ConcurrentHashMap<Level, Icon>();
    }

    @Override
    public Component getListCellRendererComponent(JList<?> list, Object value, int index, boolean isSelected,
            boolean cellHasFocus) {
        JLabel label = (JLabel) super.getListCellRendererComponent(list, value, index, isSelected, cellHasFocus);
        Icon icon = null;
        if (value instanceof Level) {
            icon = icons.get(value);
            if (Level.WARN.equals(value)) {
                label.setText("warning");
            } else {
                label.setText(label.getText().toLowerCase());
            }
        }
        label.setIcon(icon);
        return label;
    }

    public void addIcon(Level level, Icon icon) {
        if (level != null) {
            icons.put(level, icon);
        }
    }

    public void removeIcon(Level level) {
        if (level != null) {
            icons.remove(level);
        }
    }

    public void clearIcons() {
        icons.clear();
    }

}
