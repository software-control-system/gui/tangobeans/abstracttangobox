package fr.soleil.comete.box;

import java.awt.Checkbox;
import java.awt.Color;
import java.awt.Component;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ContainerEvent;
import java.awt.event.ContainerListener;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;
import java.util.prefs.Preferences;

import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;

import org.jdesktop.swingx.event.WeakEventListenerList;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fr.soleil.comete.box.matrixbox.StringMatrixBox;
import fr.soleil.comete.box.scalarbox.BooleanScalarBox;
import fr.soleil.comete.box.scalarbox.NumberScalarBox;
import fr.soleil.comete.box.scalarbox.StringScalarBox;
import fr.soleil.comete.box.target.ADeviceLogAdapter;
import fr.soleil.comete.box.target.DeviceLogMatrixAdapter;
import fr.soleil.comete.box.target.DeviceLogSpectrumAdapter;
import fr.soleil.comete.box.util.DeviceLabel;
import fr.soleil.comete.box.util.DeviceLogViewer;
import fr.soleil.comete.box.util.SourceReadingDelegate;
import fr.soleil.comete.box.util.TangoBoxConstants;
import fr.soleil.comete.definition.data.target.scalar.ITextComponent;
import fr.soleil.comete.definition.widget.IComponent;
import fr.soleil.comete.definition.widget.util.CometeColor;
import fr.soleil.comete.definition.widget.util.CometeFont;
import fr.soleil.comete.service.CometeBoxProvider;
import fr.soleil.comete.swing.BooleanComboBox;
import fr.soleil.comete.swing.CheckBox;
import fr.soleil.comete.swing.Label;
import fr.soleil.comete.swing.StringButton;
import fr.soleil.comete.swing.TextField;
import fr.soleil.comete.swing.WheelSwitch;
import fr.soleil.comete.tango.data.service.AttributePropertyType;
import fr.soleil.comete.tango.data.service.TangoDataSourceFactory;
import fr.soleil.comete.tango.data.service.TangoKey;
import fr.soleil.comete.tango.data.service.TangoKeyTool;
import fr.soleil.comete.tango.data.service.helper.TangoExceptionHelper;
import fr.soleil.data.mediator.Mediator;
import fr.soleil.data.service.AbstractRefreshingManager;
import fr.soleil.data.service.CompositeKey;
import fr.soleil.data.service.DataSourceProducerProvider;
import fr.soleil.data.service.IDataSourceProducer;
import fr.soleil.data.service.IKey;
import fr.soleil.data.source.AbstractDataSource;
import fr.soleil.data.target.ITarget;
import fr.soleil.data.target.scalar.IBooleanTarget;
import fr.soleil.data.target.scalar.INumberTarget;
import fr.soleil.data.target.scalar.IScalarTarget;
import fr.soleil.data.target.scalar.ITextTarget;
import fr.soleil.lib.project.ObjectUtils;
import fr.soleil.lib.project.application.logging.LogViewer;
import fr.soleil.lib.project.progress.IProgressable;
import fr.soleil.lib.project.progress.IProgressionListener;
import fr.soleil.lib.project.progress.Startable;
import fr.soleil.lib.project.resource.MessageManager;
import fr.soleil.lib.project.swing.ITextTransferable;
import fr.soleil.lib.project.swing.TextTansfertHandler;
import fr.soleil.lib.project.swing.icons.Icons;

/**
 * This class is a Comete based alternative to SoleilUtilities AbstractBean.
 * 
 * @author girardot
 */
public abstract class AbstractTangoBox extends JPanel
        implements Startable, IProgressable, ContainerListener, ITextTransferable, TangoBoxConstants {

    private static final long serialVersionUID = 7635267175203012680L;

    public static final String NO_DEVICE_STRING = "no device defined";
    protected static final String STATUS = "Status";
    protected static final String STATE = "State";
    protected static final String AT = "@";
    protected static final String THREAD_NAME_START = " connection to '";
    protected static final String SINGLE_QUOT = "'";
    protected static final String DEVICE = "device";
    protected static final String PROPERTIES_EXT = ".properties";
    protected static final String CONNECTION_TO = "Connection to ";
    protected static final String DONE = " done";
    protected static final String WON_T_START = " won't start because empty model or clearing connections: model='";
    protected static final String IS_CLEARING_CONNECTIONS = "', isClearingConnections=";
    protected static final String CLEAR_CONNECTIONS = " Clear connections";
    protected static final String ONE = "1";
    protected static final String DEVICE_PKG = ".device";
    protected static final String ICONS_PKG = ".icons";
    protected static final String MESSAGES_PKG = ".messages";
    protected static final String CLEAR_LOGS_CONFIRMATION_MESSAGE = "tangobox.log.device.clear.confirmation.message";
    protected static final String CLEAR_LOGS_CONFIRMATION_TITLE = "tangobox.log.device.clear.confirmation.title";
    protected static final String COMMAND = ": Command ";
    protected static final String ATTRIBUTE = ": Attribute ";
    protected static final String HAS_BEEN_IGNORED_AND_WON_T_BE_CONNECTED_TO = " has been ignored and won't be connected to ";
    protected static final String TRUE = "true";
    protected static final String FALSE = "false";
    protected static final String ATTRIBUTES = "attributes";
    protected static final String COMMANDS = "commands";
    protected static final String CONNECTION_ERROR = "Connection Error";
    protected static final String ENTITIES = "entities";
    protected static final String NEW_LINES_3 = "\n\n\n";
    protected static final String TITLE_SEPARATOR = ":\n";
    protected static final String COULD_NOT_CONNECT_TO_THE_FOLLOWING = "Could not connect to the following ";
    protected static final String OPEN_PARENTHESIS = " (";
    protected static final String CLOSE_PARENTHESIS = "):\n";
    protected static final String SEPARATOR_NEW_LINE = "--------------------\n";
    protected static final String COMMA = ", ";
    protected static final String NEW_LINE = "\n";
    protected static final String NEW_LINE_SEPARATOR = "\n--------------------";

    // resources
    protected ResourceBundle deviceBundle;
    protected final ResourceBundle resourceBundle;
    protected final Icons iconsManager;

    // attributes and commands management
    protected List<String> badAttributes;
    protected List<String> allowedBadAttributes;
    protected List<String> badCommands;
    protected List<String> allowedBadCommands;
    // JIRA CONTROLGUI-95
    protected List<String> ignoredBadAttributes;
    protected List<String> ignoredBadCommands;

    // progression
    protected WeakEventListenerList progressionListenerList;
    protected int maxProgress;
    protected int currentProgression;

    // refresh management
    protected volatile boolean stopped;
    protected boolean error;
    protected int refreshingTime;
    protected final Object runLock;

    /**
     * The default refreshing time for newly created beans.
     */
    protected static int defaultRefreshingTime = 1000;

    protected ConcurrentMap<ITarget, AbstractCometeBox<?>> connectedWidgetMap = new ConcurrentHashMap<>();

    // what this bean should connect to
    protected String model;

    // status bar
    protected JPanel statusPanel;
    protected JLabel deviceLabel;
    protected Label statusLabel;
    protected Label stateLabel;
    private final DeviceLabel deviceStatusLabel;

    // CometeBox
    protected final NumberScalarBox numberBox;
    protected final BooleanScalarBox booleanBox;
    protected final StringScalarBox stringBox;

    // Connection threads
    private volatile Thread guiThread;
    private volatile Thread killThread;
    private volatile boolean canceled;
    private boolean manageChildrenStartStopAndRefresh;

    /**
     * Constructor
     */
    public AbstractTangoBox() {
        super();
        TextTansfertHandler.registerTransferHandler(this);
        canceled = false;
        manageChildrenStartStopAndRefresh = false;
        // Each AbstractTangoBox uses its own CometeBox in case of need for custom configuration
        stringBox = new StringScalarBox();
        numberBox = new NumberScalarBox();
        booleanBox = new BooleanScalarBox();
        killThread = null;
        guiThread = null;
        stopped = true;
        error = false;
        runLock = new Object();
        model = ObjectUtils.EMPTY_STRING;
        deviceLabel = new JLabel(NO_DEVICE_STRING);
        statusLabel = new Label();
        stateLabel = getStateLabel();
        deviceStatusLabel = new DeviceLabel();
        deviceStatusLabel.setDeviceName(NO_DEVICE_STRING);

        progressionListenerList = new WeakEventListenerList();
        initMaxProgress();

        // Resource for GUI
        resourceBundle = createResourceBundle();
        // Icons for GUI
        iconsManager = createIconBundle();

        initialize();

        badAttributes = new ArrayList<>();
        allowedBadAttributes = new ArrayList<>();
        badCommands = new ArrayList<>();
        allowedBadCommands = new ArrayList<>();
        ignoredBadAttributes = new ArrayList<>();
        ignoredBadCommands = new ArrayList<>();

        refreshingTime = defaultRefreshingTime;

        // bean information transmission
        addContainerListener(this);
    }

    @Override
    public String getTextToTransfert() {
        return model;
    }

    /**
     * Returns whether this {@link AbstractTangoBox} will manage the starting, stopping and refreshing period of its
     * contained {@link AbstractTangoBox}es
     * 
     * @return A <code>boolean</code> value. <code>false</code> by default
     */
    public boolean isManageChildrenStartStopAndRefresh() {
        return manageChildrenStartStopAndRefresh;
    }

    /**
     * Sets whether this {@link AbstractTangoBox} should manage the starting, stopping and refreshing period of its
     * contained {@link AbstractTangoBox}es
     * 
     * @param manageChildrenStartAndStop Whether this {@link AbstractTangoBox} should manage the starting, stopping and
     *            refreshing period of contained {@link AbstractTangoBox}es
     */
    public void setManageChildrenStartStopAndRefresh(boolean manageChildrenStartAndStop) {
        this.manageChildrenStartStopAndRefresh = manageChildrenStartAndStop;
    }

    /**
     * Returns whether device connection is canceled
     * 
     * @return A <code>boolean</code> value
     */
    public final boolean isCanceled() {
        return canceled;
    }

    protected IDataSourceProducer getProducer() {
        return DataSourceProducerProvider.getProducer(TangoDataSourceFactory.SOURCE_PRODUCER_ID);
    }

    /**
     * Used by the bean to transmit resfreshingTime, start/stop to its children
     * 
     * @see AbstractTangoBox#setRefreshingTime(int)
     * @see #start()
     * @see #stop()
     */
    @Override
    public final void componentAdded(ContainerEvent e) {
        if (e != null) {
            Component comp = e.getChild();
            if (isManageChildrenStartStopAndRefresh()) {
                // transmit refreshing time, start/stop
                setRefreshingTime(getRefreshingTime(), comp);
                if (stopped) {
                    stop(comp);
                } else {
                    start(comp);
                }
            }
            // Check Containers but not AbstractTangoBoxes, as AbstractTangoBoxes manage their own children.
            // Doing so avoids having the same event and its consequences happening too many times.
            if ((comp instanceof Container) && (!(comp instanceof AbstractTangoBox))) {
                ((Container) comp).addContainerListener(this);
            }
        }
    }

    @Override
    public final void componentRemoved(ContainerEvent e) {
        if (e != null) {
            Component comp = e.getChild();
            // Stop listening to the container
            if ((comp instanceof Container) && (!(comp instanceof AbstractTangoBox))) {
                ((Container) comp).removeContainerListener(this);
            }
        }
    }

    /**
     * Connects the status panel and label to the device represented by {@link #getModel()}
     * 
     * @see #getStatusPanel()
     * @see #getDeviceStatusLabel()
     * @see #setModel(String)
     * @see #getModel()
     * @see #cleanStatusModel()
     */
    protected void setStatusModel() {
        deviceLabel.setText(getModel());
        deviceStatusLabel.setDeviceName(getModel());
        IKey statusKey = generateAttributeKey(STATUS);
        setWidgetModel(statusLabel, stringBox, statusKey);
        setWidgetModel(deviceStatusLabel, stringBox, statusKey);
    }

    /**
     * Connects the state Label to the device represented by {@link #getModel()}
     * 
     * @see #getStateLabel()
     * @see #setModel(String)
     * @see #getModel()
     * @see #cleanStateModel()
     */
    protected void setStateModel() {
        setWidgetModel(getStateLabel(), CometeBoxProvider.getCometeBox(StringScalarBox.class),
                generateAttributeKey(STATE));
    }

    /**
     * Disconnects the state label from its associated device
     * 
     * @see #getStateLabel()
     * @see #setModel(String)
     * @see #getModel()
     * @see #setStateModel()
     */
    protected void cleanStateModel() {
        cleanWidget(getStateLabel());
    }

    /**
     * Disconnects the status panel and label from their associated device
     * 
     * @see #getStatusPanel()
     * @see #getDeviceStatusLabel()
     * @see #setModel(String)
     * @see #getModel()
     * @see #setStatusModel()
     */
    protected void cleanStatusModel() {
        deviceLabel.setText(NO_DEVICE_STRING);
        deviceStatusLabel.setDeviceName(NO_DEVICE_STRING);
        cleanWidget(statusLabel);
        cleanWidget(deviceStatusLabel);
        statusLabel.setOpaque(false);
        deviceStatusLabel.setOpaque(false);
    }

    /**
     * Returns the state label. The state label is a {@link JLabel} that informs about the state of a Tango device
     * 
     * @return a {@link JLabel}
     * @see #setStateModel()
     * @see #cleanStateModel()
     */
    public Label getStateLabel() {
        if (stateLabel == null) {
            stateLabel = new Label();
            stateLabel.setHorizontalAlignment(JLabel.CENTER);
        }
        return stateLabel;
    }

    /**
     * Returns the status panel. The status panel is a {@link JPanel} that informs about the name and status of a Tango
     * device
     * 
     * @return a {@link JPanel}
     * @see #setStatusModel()
     * @see #cleanStatusModel()
     */
    public JPanel getStatusPanel() {
        if (statusPanel == null) {
            statusPanel = new JPanel(new GridBagLayout());
            GridBagConstraints deviceConstraints = new GridBagConstraints();
            deviceConstraints.fill = GridBagConstraints.VERTICAL;
            deviceConstraints.gridx = 0;
            deviceConstraints.gridy = 0;
            deviceConstraints.weightx = 0;
            deviceConstraints.weighty = 1;
            deviceConstraints.insets = new Insets(0, 5, 0, 5);
            statusPanel.add(deviceLabel, deviceConstraints);
            GridBagConstraints statusConstraints = new GridBagConstraints();
            statusConstraints.fill = GridBagConstraints.BOTH;
            statusConstraints.gridx = 1;
            statusConstraints.gridy = 0;
            statusConstraints.weightx = 1;
            statusConstraints.weighty = 1;
            statusConstraints.insets = new Insets(0, 0, 0, 0);
            statusPanel.add(statusLabel, statusConstraints);
        }
        return statusPanel;
    }

    /**
     * Returns a {@link DeviceLabel}, that should be connected to this {@link AbstractTangoBox} device
     * 
     * @return A {@link DeviceLabel}
     */
    public DeviceLabel getDeviceStatusLabel() {
        return deviceStatusLabel;
    }

    /**
     * initialization
     */
    private void initialize() {
        setSize(300, 200);
    }

    /**
     * Returns the name of the device this {@link AbstractTangoBox} is associated with
     * 
     * @return a {@link String}
     */
    public String getModel() {
        return model;
    }

    /**
     * Associates a Tango device with this {@link AbstractTangoBox}. The connection will be done in {@link #start()}
     * method.
     * 
     * @param model the device name
     */
    public void setModel(String model) {
        synchronized (runLock) {
            boolean started = false;
            // Protect from null reference
            if (model == null) {
                model = ObjectUtils.EMPTY_STRING;
            } else {
                model = model.trim();
            }
            // update model only if it is not the same one
            if (!model.equalsIgnoreCase(this.model)) {
                // clearConnection();
                started = !stopped;
                // Stop Refreshing
                if (started) {
                    stop();
                }
                this.model = model;
                if (this.model.isEmpty()) {
                    callSavePreferences();
                }
            }
            if (started) {
                start();
            }
        }
    }

    @Override
    public void start() {
        synchronized (runLock) {
            if (stopped) {
                stopped = false;
                if ((guiThread == null) || (!guiThread.isAlive()) || guiThread.isInterrupted()) {
                    if ((killThread != null) && (killThread.isAlive())) {
                        try {
                            killThread.join();
                        } catch (InterruptedException e) {
                            // ignore this exception
                        }
                    }
                    canceled = false;
                    guiThread = new Thread(getDefaultToString() + THREAD_NAME_START + getModel() + SINGLE_QUOT) {
                        @Override
                        public void run() {
                            // refreshing management is done here
                            if ((!getModel().isEmpty()) && (!isCanceled())) {
                                error = false;
                                try {
                                    // testing "Status" attribute, as it will be called anyway in setStatusModel()
                                    // method
                                    if (!checkAttribute(STATUS)) {
                                        error = true;
                                        onConnectionError();
                                    }
                                } catch (Exception e) {
                                    LoggerFactory.getLogger(Mediator.LOGGER_ACCESS)
                                            .error("Unexpected error while checking " + getModel() + " status in "
                                                    + guiThread.getName(), e);
                                    error = true;
                                    onConnectionError();
                                }
                                try {
                                    // Resource Interface of the device
                                    deviceBundle = createDeviceBundle();
                                } catch (Throwable t) {
                                    error = true;
                                    LoggerFactory.getLogger(Mediator.LOGGER_ACCESS)
                                            .error("Unexpected error while recovering device ResourceBundle in "
                                                    + guiThread.getName(), t);
                                    AbstractTangoBox.this.stop();
                                }
                            }
                            if (isManageChildrenStartStopAndRefresh()) {
                                for (Component comp : getComponents()) {
                                    AbstractTangoBox.this.start(comp);
                                }
                            }
                            if ((!getModel().isEmpty()) && (!isCanceled())) {
                                currentProgression = 0;
                                try {
                                    refreshGUI();
                                } finally {
                                    fireProgressionChanged(getMaxProgression(), CONNECTION_TO + getModel() + DONE);
                                }
                                // Load Preferences
                                callLoadPreferences();
                            } else {
                                LoggerFactory.getLogger(Mediator.LOGGER_ACCESS)
                                        .trace(AbstractTangoBox.this.getClass().getName() + WON_T_START + getModel()
                                                + IS_CLEARING_CONNECTIONS + isCanceled());
                            }
                        }
                    };
                    guiThread.start();
                }
            }
        }
    }

    @Deprecated
    protected final void clearConnection() {
        synchronized (runLock) {
            clearGUI();
        }
    }

    @Override
    public void stop() {
        synchronized (runLock) {
            if (!stopped) {
                stopped = true;
                if ((killThread == null) || (!killThread.isAlive())) {
                    killThread = new Thread(getDefaultToString() + CLEAR_CONNECTIONS) {
                        @Override
                        public void run() {
                            canceled = true;
                            if ((guiThread != null) && (guiThread.isAlive())) {
                                try {
                                    guiThread.join();
                                } catch (InterruptedException e1) {
                                    // ignore
                                }
                            }
                            clearGUI();
                            if (isManageChildrenStartStopAndRefresh()) {
                                for (Component comp : getComponents()) {
                                    AbstractTangoBox.this.stop(comp);
                                }
                            }
                            repaint();
                        }
                    };
                    killThread.start();
                }
            }
        }
    }

    /**
     * Transmits the "start" to the Children {@link AbstractTangoBox}s of a Component.
     * 
     * @param comp the component
     */
    protected final void start(Component comp) {
        if (!isCanceled()) {
            if (comp instanceof AbstractTangoBox) {
                ((AbstractTangoBox) comp).start();
            } else if (comp instanceof Container) {
                Container container = (Container) comp;
                for (Component child : container.getComponents()) {
                    start(child);
                }
            }
        }
    }

    /**
     * Transmits the "stop" to the Children {@link AbstractTangoBox}s of a Component.
     * 
     * @param comp the component
     */
    protected final void stop(Component comp) {
        if (comp instanceof AbstractTangoBox) {
            ((AbstractTangoBox) comp).stop();
        } else if (comp instanceof Container) {
            Container container = (Container) comp;
            for (Component child : container.getComponents()) {
                stop(child);
            }
        }
    }

    /**
     * Clear GUI This method is call by setModel() when the model is empty
     */
    protected abstract void clearGUI();

    /**
     * Refresh GUI This method is call by setModel() when the model is not empty
     */
    protected abstract void refreshGUI();

    /**
     * Do anything you want when the model isn't accessible
     * 
     * This method is call by start() when it tries to connect to device
     * 
     */
    protected abstract void onConnectionError();

    /**
     * This method return the version of the model. It must be only use in Online mode
     * 
     * @return the version of the Class model
     */
    protected String getVersion() {
        return ONE;
    }

    private ResourceBundle createDeviceBundle() {
        ResourceBundle result = null;
        try {
            result = ResourceBundle.getBundle(getClass().getPackage().getName() + DEVICE_PKG + getVersion());
        } catch (Exception e) {
            result = null;
        }
        return result;
    }

    /**
     * Creates the {@link Icons} this {@link AbstractTangoBox} will use
     * 
     * @return an {@link Icons}
     */
    protected Icons createIconBundle() {
        try {
            return new Icons(getClass().getPackage().getName() + ICONS_PKG);
        } catch (Exception e) {
            return null;
        }
    }

    /**
     * Create resource bundle for this bean
     * 
     * @return a {@link ResourceBundle}
     */
    protected ResourceBundle createResourceBundle() {
        ResourceBundle result = null;
        try {
            result = ResourceBundle.getBundle(getClass().getPackage().getName() + MESSAGES_PKG);
        } catch (Exception e) {
            result = null;
        }
        return result;
    }

    /**
     * Saves preferences the default way
     */
    private void callSavePreferences() {
        savePreferences(Preferences.userNodeForPackage(getClass()));
    }

    /**
     * Loads preferences the default way
     */
    private void callLoadPreferences() {
        loadPreferences(Preferences.userNodeForPackage(getClass()));
    }

    /**
     * Save what you want into Preferences System Call when Model is setting to null
     */
    protected abstract void savePreferences(final Preferences preferences);

    /**
     * Load what you want into Preferences System Call when Model is setting
     */
    protected abstract void loadPreferences(final Preferences preferences);

    @Override
    public void paint(final Graphics g) {
        super.paint(g);
        if (error) {
            // Paints an error cross in case of error
            Dimension size = super.getSize();
            Color former = g.getColor();
            g.setColor(Color.red);
            g.drawLine(0, 0, size.width, size.height);
            g.drawLine(size.width, 0, 0, size.height);
            g.setColor(former);
            former = null;
        }
    }

    /**
     * Returns the list of attributes that were checked as invalid
     * 
     * @return a {@link String} {@link List}
     */
    public List<String> getBadAttributes() {
        return badAttributes;
    }

    /**
     * Returns the list of commands that were checked as invalid
     * 
     * @return a {@link String} {@link List}
     */
    public List<String> getBadCommands() {
        return badCommands;
    }

    /**
     * Returns the list of attributes that are allowed to be invalid (example: attributes that exist on some devices,
     * but do not exist on some others)
     * 
     * @return a {@link String} {@link List}
     */
    public List<String> getAllowedBadAttributes() {
        return allowedBadAttributes;
    }

    /**
     * Sets the list of attributes that are allowed to be invalid (example: attributes that exist on some devices, but
     * do not exist on some others)
     * 
     * @param allowedBadAttributes the {@link List} to set
     */
    public void setAllowedBadAttributes(List<String> allowedBadAttributes) {
        if (allowedBadAttributes == null) {
            allowedBadAttributes = new ArrayList<>();
        }
        this.allowedBadAttributes = allowedBadAttributes;
    }

    /**
     * Returns the list of commands that are allowed to be invalid (example: commands that exist on some devices, but do
     * not exist on some others)
     * 
     * @return a {@link String} {@link List}
     */
    public List<String> getAllowedBadCommands() {
        return allowedBadCommands;
    }

    /**
     * Sets the list of commands that are allowed to be invalid (example: commands that exist on some devices, but do
     * not exist on some others)
     * 
     * @param allowedBadCommands the {@link List} to set
     */
    public void setAllowedBadCommands(List<String> allowedBadCommands) {
        if (allowedBadCommands == null) {
            allowedBadCommands = new ArrayList<>();
        }
        this.allowedBadCommands = allowedBadCommands;
    }

    /**
     * Initializes the maximum progression value
     * 
     * @see #getMaxProgression()
     */
    protected void initMaxProgress() {
        currentProgression = 0;
        maxProgress = 0;
    }

    @Override
    public void addProgressionListener(final IProgressionListener listener) {
        if (listener != null) {
            // ensure uniqueness
            progressionListenerList.remove(IProgressionListener.class, listener);
            progressionListenerList.add(IProgressionListener.class, listener);
        }
    }

    @Override
    public void removeProgressionListener(final IProgressionListener listener) {
        if (listener != null) {
            progressionListenerList.remove(IProgressionListener.class, listener);
        }
    }

    protected void progress(boolean warnProgress, String info) {
        if (warnProgress) {
            fireProgressionChanged(++currentProgression, info);
        }
    }

    /**
     * Warns listeners about a change in progression
     * 
     * @param progression The progression value
     * @param info The progression message
     */
    protected void fireProgressionChanged(final int progression, final String info) {
        IProgressionListener[] listeners = progressionListenerList.getListeners(IProgressionListener.class);
        for (IProgressionListener listener : listeners) {
            listener.progressionChanged(this, progression, info);
        }
    }

    /**
     * Returns the maximum progression value for this bean (used with progress bars, splash, etc...)
     * 
     * @return an int
     */
    public int getMaxProgression() {
        return maxProgress;
    }

    /**
     * @return the refreshingTime
     */
    public int getRefreshingTime() {
        return refreshingTime;
    }

    /**
     * Sets the refreshing time of this {@link AbstractTangoBox}. Not yet supported.
     * 
     * @param refreshingTime the refreshingTime to set
     */
    public void setRefreshingTime(final int milliSeconds) {
        refreshingTime = milliSeconds;
        if (isManageChildrenStartStopAndRefresh()) {
            for (int i = 0; i < getComponentCount(); i++) {
                setRefreshingTime(milliSeconds, getComponent(i));
            }
        }
    }

    /**
     * Recursively transmits the refreshing time to a {@link Component} and its descendants
     * 
     * @param milliSeconds the refreshingTime to set
     * @param comp the {@link Component}
     */
    protected final void setRefreshingTime(final int milliSeconds, Component comp) {
        if (comp instanceof AbstractTangoBox) {
            ((AbstractTangoBox) comp).setRefreshingTime(milliSeconds);
        } else if (comp instanceof Container) {
            Container container = (Container) comp;
            for (Component child : container.getComponents()) {
                setRefreshingTime(milliSeconds, child);
            }
        }
    }

    /**
     * Getter for the default refreshing time.
     * 
     * @return defaultRefreshingTime
     */
    public static int getDefaultRefreshingTime() {
        return defaultRefreshingTime;
    }

    /**
     * Setter for the default refreshing time.
     * 
     * @param defaultRefreshingTime
     */
    public static void setDefaultRefreshingTime(final int defaultRefreshingTime) {
        AbstractTangoBox.defaultRefreshingTime = defaultRefreshingTime;
    }

    /**
     * Connects the "clear device logs" button of a {@link DeviceLogViewer} to the expected "ClearLogs" command.
     * 
     * @param viewer The {@link DeviceLogViewer}.
     * @param clearLogCommandKey The "ClearLog" command {@link IKey}.
     * @param stringBox The {@link StringScalarBox} that will do the connection.
     * @param messageManager The {@link MessageManager} that knows the messages to display.
     */
    public static void connectClearDeviceLogsButton(DeviceLogViewer viewer, IKey clearLogCommandKey,
            StringScalarBox stringBox, MessageManager messageManager) {
        if ((viewer != null) && (clearLogCommandKey != null) && (stringBox != null)) {
            MessageManager msgManager = messageManager == null ? DEFAULT_MESSAGE_MANAGER : messageManager;
            stringBox.setErrorText(viewer.getClearDeviceLogsButton(), ObjectUtils.EMPTY_STRING);
            stringBox.setUseErrorText(viewer.getClearDeviceLogsButton(), false);
            stringBox.connectWidget(viewer.getClearDeviceLogsButton(), clearLogCommandKey);
            stringBox.setConfirmation(viewer.getClearDeviceLogsButton(), true);
            stringBox.setConfirmationMessage(viewer.getClearDeviceLogsButton(),
                    msgManager.getMessage(CLEAR_LOGS_CONFIRMATION_MESSAGE));
            stringBox.setConfirmationTitle(viewer.getClearDeviceLogsButton(),
                    msgManager.getMessage(CLEAR_LOGS_CONFIRMATION_TITLE));
        }
    }

    /**
     * Recovers the best {@link ADeviceLogAdapter} for given {@link LogViewer} and log attribute {@link IKey}.
     * 
     * @param viewer The {@link LogViewer}.
     * @param logAttributeKey The log attribute {@link IKey}.
     * @return An {@link ADeviceLogAdapter}.
     */
    public static ADeviceLogAdapter<?> getBestDeviceLogAdapter(LogViewer viewer, IKey logAttributeKey) {
        ADeviceLogAdapter<?> adapter;
        if ((logAttributeKey != null) && (viewer != null)) {
            IDataSourceProducer producer = DataSourceProducerProvider.getProducer(logAttributeKey);
            if (producer == null) {
                adapter = null;
            } else {
                int rank = producer.getRank(logAttributeKey);
                switch (rank) {
                    case 1:
                        adapter = new DeviceLogSpectrumAdapter(viewer);
                        break;
                    case 2:
                        adapter = new DeviceLogMatrixAdapter(viewer);
                        break;
                    default:
                        adapter = null;
                        break;
                }
            }
        } else {
            adapter = null;
        }
        return adapter;
    }

    /**
     * Recovers the best {@link ADeviceLogAdapter} for given {@link LogViewer} and log attribute {@link IKey} and
     * potentially connects it through given {@link StringMatrixBox}.
     * 
     * @param viewer The {@link LogViewer}.
     * @param logAttributeKey The log attribute {@link IKey}.
     * @param stringMatrixBox The {@link StringMatrixBox}.
     * @param connect Whether to connect the resulting {@link ADeviceLogAdapter}.
     * @return An {@link ADeviceLogAdapter}.
     */
    public static ADeviceLogAdapter<?> getBestDeviceLogAdapter(LogViewer viewer, IKey logAttributeKey,
            StringMatrixBox stringMatrixBox, boolean connect) {
        ADeviceLogAdapter<?> adapter = getBestDeviceLogAdapter(viewer, logAttributeKey);
        if (connect && (adapter != null)) {
            stringMatrixBox.connectWidget(adapter, logAttributeKey);
        }
        return adapter;
    }

    /**
     * Recovers the best {@link ADeviceLogAdapter} for given {@link LogViewer} and log attribute {@link IKey} and
     * connects it through given {@link StringMatrixBox}.
     * 
     * @param viewer The {@link LogViewer}.
     * @param logAttributeKey The log attribute {@link IKey}.
     * @param stringMatrixBox The {@link StringMatrixBox}.
     * @return An {@link ADeviceLogAdapter}.
     */
    public static ADeviceLogAdapter<?> getAndConnectBestDeviceLogAdapter(LogViewer viewer, IKey logAttributeKey,
            StringMatrixBox stringMatrixBox) {
        return getBestDeviceLogAdapter(viewer, logAttributeKey, stringMatrixBox, true);
    }

    /**
     * Generates the {@link TangoKey} that corresponds to an attribute in the associated device
     * 
     * @param attributeShortName the attribute short name (without device name)
     * @return The expected {@link TangoKey}
     */
    public TangoKey generateAttributeKey(String attributeShortName) {
        TangoKey key = new TangoKey();
        TangoKeyTool.registerAttribute(key, getModel(), attributeShortName);
        return key;
    }

    /**
     * Generates the {@link TangoKey} that corresponds to an attribute in the associated device, and for which you
     * expect the data source to be read only.
     * 
     * @param attributeShortName the attribute short name (without device name)
     * @return The expected {@link TangoKey}
     */
    public TangoKey generateReadOnlyAttributeKey(String attributeShortName) {
        TangoKey key = generateAttributeKey(attributeShortName);
        TangoKeyTool.registerSettable(key, false);
        return key;
    }

    /**
     * Generates the {@link TangoKey} that corresponds to an attribute in the associated device
     * 
     * @param attributeShortName the attribute short name (without device name)
     * @return The expected {@link TangoKey}
     */
    public TangoKey generateWriteAttributeKey(String attributeShortName) {
        TangoKey key = new TangoKey();
        TangoKeyTool.registerWriteAttribute(key, getModel(), attributeShortName);
        return key;
    }

    /**
     * Generates the {@link TangoKey} that corresponds to a list of attributes
     * 
     * @param completeNames The attributes complete names (example: "tango/tangotest/1/short_scalar_ro")
     * @return The expected {@link TangoKey}
     */
    public List<IKey> generateMultiAttributeKey(String... completeNames) {

        List<IKey> result = new ArrayList<>();
        if (completeNames != null) {
            for (String attName : completeNames) {
                result.add(generateAttributeKey(attName));
            }
        }
        return result;
    }

    /**
     * Generates the {@link TangoKey} that corresponds to attributes couples.
     * This means that the generated {@link TangoKey} represents a list of {attribute VS attribute}. This is typically
     * used in a chart, with one attribute in X and one in Y coordinates for each curve.
     * 
     * @param xCompleteNames The complete names of the attributes in X
     * @param yCompleteNames The complete names of the attributes in Y
     * @return The expected {@link TangoKey}
     */
    public List<IKey> generateDualAttributeKey(String[] xCompleteNames, String[] yCompleteNames) {
        List<IKey> result = new ArrayList<>();
        if ((xCompleteNames != null) && (yCompleteNames != null)) {
            for (int i = 0; i < xCompleteNames.length; i++) {
                String xAttr = xCompleteNames[i];
                String yAttr = yCompleteNames[i];

                TangoKey keyX = new TangoKey();
                TangoKeyTool.registerAttribute(keyX, xAttr);

                TangoKey keyY = new TangoKey();
                TangoKeyTool.registerAttribute(keyY, yAttr);

                CompositeKey compKey = new CompositeKey();
                compKey.addKey(CompositeKey.X_INDEX, keyX);
                compKey.addKey(CompositeKey.Y_INDEX, keyY);

                result.add(compKey);
            }
        }
        return result;
    }

    /**
     * Generates the {@link TangoKey} that corresponds to an attribute property in the associated device
     * 
     * @param attributeShortName the attribute short name (without device name)
     * @param propertyType the property
     * @return The expected {@link TangoKey}
     */
    public TangoKey generateAttributePropertyKey(String attributeShortName, AttributePropertyType propertyType) {
        TangoKey key = new TangoKey();
        TangoKeyTool.registerAttributeProperty(key, getModel(), attributeShortName, propertyType);
        return key;
    }

    /**
     * Generates the {@link TangoKey} that corresponds to a command in the associated device
     * 
     * @param commandShortName the command short name (without device name)
     * @return The expected {@link TangoKey}
     */
    public TangoKey generateCommandKey(String commandShortName) {
        TangoKey key = new TangoKey();
        TangoKeyTool.registerCommand(key, getModel(), commandShortName);
        return key;
    }

    /**
     * Generates the {@link TangoKey} that corresponds to a property in the associated device
     * 
     * @param propertyName the name of the device property
     * @return The expected {@link TangoKey}
     */
    public TangoKey generatePropertyKey(String propertyName) {
        TangoKey key = new TangoKey();
        TangoKeyTool.registerDeviceProperty(key, getModel(), propertyName);
        return key;
    }

    /**
     * Generates the {@link TangoKey} that corresponds to the associated device
     * 
     * @return The expected {@link TangoKey}
     */
    public TangoKey generateDeviceKey() {
        TangoKey key = new TangoKey();
        TangoKeyTool.registerDeviceName(key, getModel());
        return key;
    }

    /**
     * Cleans an {@link ITarget} (disconnects it from all sources) in a separated {@link Thread}
     * 
     * @param widget the {@link ITarget} to clean
     */
    @SuppressWarnings("unchecked")
    protected <T extends ITarget> void cleanWidget(final T widget) {
        AbstractCometeBox<T> cometeBox;
        if (widget != null) {
            cometeBox = (AbstractCometeBox<T>) connectedWidgetMap.remove(widget);

            if (cometeBox != null) {
                cometeBox.disconnectWidgetFromAll(widget);
            }
        }
    }

    protected void cleanWidgets(boolean warnProgress, String info, ITarget... widgets) {
        for (ITarget widget : widgets) {
            cleanWidget(widget);
        }
        progress(warnProgress, info);
    }

    /**
     * Connects an {@link ITarget} with a source identified by an {@link IKey} in a separated {@link Thread}
     * 
     * @param widget the {@link ITarget}
     * @param key the {@link IKey}
     * @param box The {@link AbstractCometeBox} used to connect the target
     */
    protected <T extends ITarget> void setWidgetModel(final T widget, final AbstractCometeBox<T> box, final IKey key) {
        if (!isCanceled()) {
            if ((widget != null) && (key != null) && (box != null) && (!stopped)) {
                connectedWidgetMap.put(widget, box);
                // Check ignored attributes and commands: JIRA CONTROLGUI-95
                String attributeName = TangoKeyTool.getAttributeName(key);
                if (attributeName == null) {
                    String commandName = TangoKeyTool.getCommandName(key);
                    if (commandName == null) {
                        box.connectWidget(widget, key);
                    } else if (ignoredBadCommands.contains(commandName)) {
                        StringBuilder builder = new StringBuilder(getClass().getSimpleName());
                        builder.append(COMMAND).append(commandName);
                        builder.append(HAS_BEEN_IGNORED_AND_WON_T_BE_CONNECTED_TO);
                        builder.append(widget == null ? null : widget.getClass().getName());
                        LoggerFactory.getLogger(Mediator.LOGGER_ACCESS).trace(builder.toString());
                    } else {
                        box.connectWidget(widget, key);
                    }
                } else if (ignoredBadAttributes.contains(attributeName)) {
                    StringBuilder builder = new StringBuilder(getClass().getSimpleName());
                    builder.append(ATTRIBUTE).append(attributeName);
                    builder.append(HAS_BEEN_IGNORED_AND_WON_T_BE_CONNECTED_TO);
                    builder.append(widget == null ? null : widget.getClass().getName());
                    LoggerFactory.getLogger(Mediator.LOGGER_ACCESS).trace(builder.toString());
                } else {
                    box.connectWidget(widget, key);
                }
            }
        }
    }

    /**
     * Connects an attribute to some targets only when necessary, using
     * {@link #setWidgetModel(ITarget, AbstractCometeBox, IKey)}
     * 
     * @param name The attribute name
     * @param warnProgress Whether to warn for progression change
     * @param info The progression message
     * @param widgets The widgets to connect
     * @return the generated {@link TangoKey}
     * @see #setWidgetModel(ITarget, AbstractCometeBox, IKey)
     */
    protected TangoKey setAttributeModel(String name, boolean warnProgress, String info, IScalarTarget... widgets) {
        TangoKey key = null;
        if (!badAttributes.contains(name)) {
            key = generateAttributeKey(name);
            for (IScalarTarget widget : widgets) {
                if (widget instanceof INumberTarget) {
                    INumberTarget iNumberComponent = (INumberTarget) widget;
                    setWidgetModel(iNumberComponent, numberBox, key);
                } else if (widget instanceof ITextTarget) {
                    ITextTarget iTextComponent = (ITextTarget) widget;
                    setWidgetModel(iTextComponent, stringBox, key);
                    if (widget instanceof StringButton) {
                        ((StringButton) widget).setText(name);
                    }
                } else if (widget instanceof IBooleanTarget) {
                    IBooleanTarget iBooleanComponent = (IBooleanTarget) widget;
                    setWidgetModel(iBooleanComponent, booleanBox, key);
                }
            }
            progress(warnProgress, info);
        }
        return key;
    }

    /**
     * Connects a command to some targets only when necessary, using
     * {@link #setWidgetModel(ITarget, AbstractCometeBox, IKey)}
     * 
     * @param name The command name
     * @param warnProgress Whether to warn for progression change
     * @param info The progression message
     * @param widgets The widgets to connect
     * @return the generated {@link TangoKey}
     * @see #setWidgetModel(ITarget, AbstractCometeBox, IKey)
     */
    protected TangoKey setCommandModel(String name, boolean warnProgress, String info, ITextTarget... widgets) {
        TangoKey key = null;
        if (!badCommands.contains(name)) {
            key = generateCommandKey(name);
            for (ITextTarget widget : widgets) {
                setWidgetModel(widget, stringBox, key);
                if (widget instanceof StringButton) {
                    ((StringButton) widget).setText(name);
                }
            }
        }
        progress(warnProgress, info);
        return key;
    }

    /**
     * Connects an attribute to some targets only when necessary, managing widgets visibility
     * 
     * @param name The attribute name
     * @param warnProgress Whether to warn for progression change
     * @param info The progression message
     * @param toHideIfNotConnected The {@link JLabel} that displays a connection error message, and should be hidden one
     *            connection done. Can be <code>null</code>
     * @param widgets The widgets to connect
     * @see #setAttributeModel(String, boolean, String, IScalarTarget...)
     * @see #setAllowedBadAttributes(List)
     */
    protected void connectAttribute(String name, boolean warnProgress, String info, JLabel toHideIfNotConnected,
            IScalarTarget... widgets) {
        if (allowedBadAttributes.contains(name)) {
            if (ignoredBadAttributes.contains(name)) {
                cleanWidgets(warnProgress, name, widgets);
                if (toHideIfNotConnected != null) {
                    toHideIfNotConnected.setVisible(false);
                }
                for (IScalarTarget widget : widgets) {
                    if (widget instanceof Component) {
                        ((Component) widget).setVisible(false);
                    }
                }
            } else {
                if (toHideIfNotConnected != null) {
                    toHideIfNotConnected.setVisible(true);
                }
                for (IScalarTarget widget : widgets) {
                    if (widget instanceof Component) {
                        ((Component) widget).setVisible(true);
                    }
                }
                setAttributeModel(name, warnProgress, info, widgets);
            }
        } else {
            setAttributeModel(name, warnProgress, info, widgets);
        }
    }

    /**
     * Connects a command to some targets only when necessary, managing widgets visibility
     * 
     * @param name The command name
     * @param warnProgress Whether to warn for progression change
     * @param info The progression message
     * @param widgets The widgets to connect
     * @see #setCommandModel(String, boolean, String, ITextComponent...)
     * @see #setAllowedBadCommands(List)
     */
    protected void connectCommand(String name, boolean warnProgress, String info, ITextComponent... widgets) {
        if (allowedBadCommands.contains(name)) {
            if (ignoredBadCommands.contains(name)) {
                cleanWidgets(warnProgress, name, widgets);
                for (IScalarTarget widget : widgets) {
                    if (widget instanceof Component) {
                        ((Component) widget).setVisible(false);
                    }
                }
            } else {
                for (IScalarTarget widget : widgets) {
                    if (widget instanceof Component) {
                        ((Component) widget).setVisible(true);
                    }
                }
                setCommandModel(name, warnProgress, info, widgets);
            }
        } else {
            setCommandModel(name, warnProgress, info, widgets);
        }
    }

    /**
     * Creates a new {@link WheelSwitch} and sets some default parameters in it
     * 
     * @return a new {@link WheelSwitch}
     */
    protected WheelSwitch generateWheelSwitch() {
        WheelSwitch wheelSwitch = new WheelSwitch();
        // wheelSwitch.setStateEnabled(false);
        wheelSwitch.setFont(new Font(Font.DIALOG, Font.PLAIN, 14));
        return wheelSwitch;
    }

    /**
     * Creates a new {@link StringButton} and sets some default parameters in it
     * 
     * @return a new {@link StringButton}
     */
    protected StringButton generateStringButton() {
        StringButton stringButton = new StringButton();
        stringButton.setButtonLook(true);
        return stringButton;
    }

    /**
     * Creates a new {@link TextField} and sets some default parameters in it
     * 
     * @return a new {@link TextField}
     */
    protected TextField generateTextField() {
        TextField textField = new TextField();
        textField.setCometeForeground(CometeColor.BLACK);
        return textField;
    }

    /**
     * Creates a new {@link Checkbox} and sets some default parameters in it
     * 
     * @return a new {@link Checkbox}
     */
    protected CheckBox generateCheckbox() {
        CheckBox checkbox = new CheckBox();
        checkbox.setTrueLabel(TRUE);
        checkbox.setFalseLabel(FALSE);
        return checkbox;
    }

    /**
     * Creates a new {@link BooleanComboBox} and sets some default parameters in
     * it
     * 
     * @return a new {@link BooleanComboBox}
     */
    protected BooleanComboBox generateBooleanComboBox() {
        BooleanComboBox booleanComboBox = new BooleanComboBox();
        booleanComboBox.setTrueLabel(TRUE);
        booleanComboBox.setFalseLabel(FALSE);
        return booleanComboBox;
    }

    /**
     * Creates a new {@link Label} and sets some default parameters in it
     * 
     * @return a new {@link Label}
     */
    protected Label generateLabel() {
        Label label = new Label();
        label.setCometeFont(CometeFont.DEFAULT_FONT);
        label.setHorizontalAlignment(IComponent.CENTER);
        label.setOpaque(true);
        return label;
    }

    /**
     * Checks for bad attributes and fills badAttributes list
     */
    protected void checkAttributes(String... attributeShortNames) {
        ignoredBadAttributes.clear();
        badAttributes.clear();
        if ((attributeShortNames != null) && (!isCanceled())) {
            for (String attributeShortName : attributeShortNames) {
                if (!checkAttribute(attributeShortName)) {
                    // JIRA CONTROLGUI-95
                    if (allowedBadAttributes.contains(attributeShortName)) {
                        ignoredBadAttributes.add(attributeShortName);
                    } else {
                        badAttributes.add(attributeShortName);
                    }

                }
            }
        }
    }

    /**
     * Checks for bad commands and fills badCommands list
     */
    protected void checkCommands(String... commandShortNames) {
        badCommands.clear();
        ignoredBadCommands.clear();
        if ((commandShortNames != null) && (!isCanceled())) {
            for (String commandShortName : commandShortNames) {
                if (!checkCommand(commandShortName)) {
                    if (allowedBadCommands.contains(commandShortName)) {
                        ignoredBadCommands.add(commandShortName);
                    } else {
                        badCommands.add(commandShortName);
                    }
                }
            }
        }
    }

    /**
     * Checks a single attribute and returns true if it is valid
     */
    protected boolean checkAttribute(String attributeShortName) {
        boolean result = true;
        if (!isCanceled()) {
            TangoKey key = generateAttributeKey(attributeShortName);
            String device = TangoKeyTool.getDeviceName(key);
            if ((device == null) || device.trim().isEmpty()) {
                result = false;
            } else {
                result = AbstractCometeBox.isSourceCreatable(key);
            }
        }
        return result;
    }

    /**
     * Checks a single command and returns true if it is valid
     */
    protected boolean checkCommand(String commandShortName) {
        boolean result = true;
        if (!isCanceled()) {
            TangoKey key = generateCommandKey(commandShortName);
            String device = TangoKeyTool.getDeviceName(key);
            if ((device == null) || device.trim().isEmpty()) {
                result = false;
            } else {
                result = AbstractCometeBox.isSourceCreatable(key);
            }
        }
        return result;
    }

    /**
     * Displays the bad attributes and commands, if any, in a popup.
     */
    protected void displayBadAttributesAndCommands() {
        ArrayList<String> entitiesInError = new ArrayList<>();
        entitiesInError.addAll(badAttributes);
        entitiesInError.removeAll(allowedBadAttributes);
        StringBuilder buffer = badEntitiesListToBuffer(null, entitiesInError, ATTRIBUTES);
        entitiesInError.clear();
        entitiesInError.addAll(badCommands);
        entitiesInError.removeAll(allowedBadCommands);
        buffer = badEntitiesListToBuffer(buffer, entitiesInError, COMMANDS);
        entitiesInError.clear();
        entitiesInError = null;
        if (buffer.length() > 0) {
            showMessageDialog(this, buffer, CONNECTION_ERROR, JOptionPane.ERROR_MESSAGE);
        }
        buffer = null;
    }

    /**
     * This is an alternative to {@link JOptionPane#showMessageDialog(Component, Object, String, int)}, with an always
     * on top dialog. This way, we are sure the dialog will always be visible, even in case of a Splash that could
     * normally hide it.
     * 
     * @param parentComponent The parent component
     * @param message The message to display
     * @param title the title
     * @param messageType the type of message
     * @see JOptionPane#showMessageDialog(Component, Object, String, int)
     */
    protected void showMessageDialog(Component parentComponent, Object message, String title, int messageType) {
        JOptionPane pane = new JOptionPane();
        pane.setMessageType(messageType);
        pane.setMessage(message);
        JDialog dialog = pane.createDialog(parentComponent, title);
        dialog.setAlwaysOnTop(true); // this is the most important part of the
        // hack
        dialog.setVisible(true);
        dialog.setAlwaysOnTop(false);
        dialog = null;
        pane = null;
    }

    /**
     * Appends a list of entity names in a StringBuilder. Used by {@link #displayBadAttributesAndCommands()}
     * 
     * @param buffer The {@link StringBuilder} to which to append the list. Can be <code>null</code>, in which case the
     *            result is a newly created {@link StringBuilder}
     * @param badList The list of entity names
     * @param entitiesType the entities type. Example: "attributes" or "commands". Can be <code>null</code>, in which
     *            case the value "entities" is used.
     * @return a {@link StringBuilder} containing the desired list of names
     * @see #displayBadAttributesAndCommands()
     */
    protected StringBuilder badEntitiesListToBuffer(StringBuilder buffer, List<String> badList, String entitiesType) {
        if (buffer == null) {
            buffer = new StringBuilder();
        }
        if (entitiesType == null) {
            entitiesType = ENTITIES;
        }
        if ((badList != null) && (badList.size() > 0)) {
            int entitiesInLine = 0;
            int maxEntitiesPerLine = 5;
            if (buffer.length() > 0) {
                buffer.append(NEW_LINES_3);
            } else {
                buffer.append(getClass().getSimpleName()).append(TITLE_SEPARATOR);
            }
            buffer.append(COULD_NOT_CONNECT_TO_THE_FOLLOWING);
            buffer.append(entitiesType).append(OPEN_PARENTHESIS);
            buffer.append(getModel()).append(CLOSE_PARENTHESIS);
            buffer.append(SEPARATOR_NEW_LINE);
            for (String command : badList) {
                buffer.append(command);
                entitiesInLine++;
                buffer.append(COMMA);
                if (entitiesInLine == maxEntitiesPerLine) {
                    buffer.append(NEW_LINE);
                    entitiesInLine = 0;
                }
            }
            int index = buffer.lastIndexOf(COMMA);
            buffer.replace(index, buffer.length(), ObjectUtils.EMPTY_STRING);
            buffer.append(NEW_LINE_SEPARATOR);
        }
        return buffer;
    }

    /**
     * Returns the default toString() result, i.e. "class name@hash code".
     * 
     * @return A {@link String}.
     */
    protected final String getDefaultToString() {
        return getClass().getName() + AT + hashCode();
    }

    /**
     * Extract some single data from a source, removing the source afterward if it was created especially for that
     * reading.
     * 
     * @param <T> The type of data to extract.
     * @param key The {@link TangoKey} from which to build the source.
     * @param delegate The {@link SourceReadingDelegate} able to extract desired data form the source.
     * @param defaultValue The default value to return in case of error or invalid data.
     * @param logger The {@link Logger} that might be able to log encountered error if any.
     * @return The extracted data.
     */
    protected <T> T extractSingleValue(TangoKey key, SourceReadingDelegate<T> delegate, T defaultValue, Logger logger) {
        T result = null;
        try {
            AbstractRefreshingManager<?> manager = (AbstractRefreshingManager<?>) DataSourceProducerProvider
                    .getProducer(key);
            AbstractDataSource<?> source = manager.getExistingSource(key);
            if (source == null) {
                source = manager.createDataSource(key);
                try {
                    if (source.isWriteBeforeRead()) {
                        source.setData(null);
                    }
                    result = delegate.readData(source);
                } finally {
                    manager.removeDataSource(key);
                }
            } else {
                result = delegate.readData(source);
            }
        } catch (Exception e) {
            if (logger != null) {
                logger.error(TangoExceptionHelper.getErrorMessage(e), e);
            }
        }
        if (result == null) {
            result = defaultValue;
        }
        return result;
    }

}
