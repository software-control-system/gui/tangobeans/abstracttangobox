package fr.soleil.comete.box.action;

import java.util.Collection;

import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.ImageIcon;

import fr.soleil.comete.swing.action.ChangeRefreshingPeriodAction;
import fr.soleil.comete.tango.data.service.TangoKeyTool;
import fr.soleil.comete.tango.data.service.helper.TangoDeviceHelper;
import fr.soleil.data.service.IKey;

/**
 * An {@link AbstractAction} used to change the timeout period of a device
 * 
 * @author girardot
 */
public class ChangeDeviceTimeOutAction extends ChangeRefreshingPeriodAction {

    private static final long serialVersionUID = 1681660189245619707L;

    protected static final String TIME_OUT_PERIOD_TEXT = "Change device timeout";
    protected static final String TIME_OUT_TOOLTIP = "Desired device timeout";
    protected static final ImageIcon TIME_OUT_ICON = new ImageIcon(
            ChangeDeviceTimeOutAction.class.getResource("/com/famfamfam/silk/hourglass.png"));

    protected String device;

    protected final boolean refreshTimeOutOnDeviceChange;

    public ChangeDeviceTimeOutAction() {
        this(false);
    }

    /**
     * Creates a new {@link ChangeDeviceTimeOutAction}
     * 
     * @param refreshTimeOutOnDeviceChange Whether to refresh time out on device change.
     *            <ul>
     *            <li>If <code>true</code>, then every time {@link #setDevice(String)} is called, the time out panel
     *            will be updated to the new device timeout period</li>
     *            <li>If <code>false</code>, then every time {@link #setDevice(String)} is called, the time out panels
     *            will transmit the timeout period to the new device</li>
     *            </ul>
     */
    public ChangeDeviceTimeOutAction(boolean refreshTimeOutOnDeviceChange) {
        super();
        this.refreshTimeOutOnDeviceChange = refreshTimeOutOnDeviceChange;
        putValue(NAME, TIME_OUT_PERIOD_TEXT);
        putValue(Action.SMALL_ICON, TIME_OUT_ICON);
        setDialogTitle(TIME_OUT_PERIOD_TEXT);
        setTextFieldTitle(TIME_OUT_PERIOD_TEXT);
        setTextFieldTooltip(TIME_OUT_TOOLTIP);
    }

    @Override
    protected RefreshingPeriodPanel generateRefreshingPeriodPanel() {
        return new TimeOutPanel();
    }

    /**
     * Changes the concerned device
     * 
     * @param device The device to which to set the timeout period
     */
    public void setDevice(String device) {
        this.device = device;
        applyOrRefreshTimeOut();
    }

    /**
     * Extracts the device from an {@link IKey} and transmits it to {@link #setDevice(String)}
     * 
     * @param key The {@link IKey} from which to recover the concerned device
     */
    @Override
    public void setKey(IKey key) {
        setDevice(TangoKeyTool.getDeviceName(key));
    }

    protected void applyOrRefreshTimeOut() {
        Collection<RefreshingPeriodPanel> panels = getInstanciatedRefreshingPeriodPanels();
        if (refreshTimeOutOnDeviceChange) {
            refreshPanelsTimeOut(panels, null);
        } else {
            applyPanelsTimeOut(panels, null);
        }
    }

    protected void applyPanelsTimeOut(Collection<RefreshingPeriodPanel> panels, RefreshingPeriodPanel ref) {
        for (RefreshingPeriodPanel panel : panels) {
            if (panel != ref) {
                ((TimeOutPanel) panel).applyLastTimeOut();
            }
        }
    }

    protected void refreshPanelsTimeOut(Collection<RefreshingPeriodPanel> panels, RefreshingPeriodPanel ref) {
        for (RefreshingPeriodPanel panel : panels) {
            if (panel != ref) {
                ((TimeOutPanel) panel).refreshTimeOut();
            }
        }
    }

    // ///////////// //
    // Inner classes //
    // ///////////// //

    protected class TimeOutPanel extends RefreshingPeriodPanel {

        private static final long serialVersionUID = -6523806404352107905L;

        public TimeOutPanel() {
            super();
            // time out expressed in minutes maximum
            for (Integer prec : TIME_PRECISION_VALUE) {
                if (prec.intValue() > 60000) {
                    precisionCombo.removeItem(prec);
                }
            }
            setPrecisionNoLock(TangoDeviceHelper.getTimeOutInMilliseconds(device), true);
        }

        public void refreshTimeOut() {
            setPrecision(TangoDeviceHelper.getTimeOutInMilliseconds(device));
        }

        @Override
        public void apply() {
            if (computePrecision()) {
                applyLastTimeOut();
                refreshPanelsTimeOut(getInstanciatedRefreshingPeriodPanels(), this);
            }
        }

        public void applyLastTimeOut() {
            TangoDeviceHelper.setTimeOutInMilliseconds(device, getPrecision());
        }
    }

}
