package fr.soleil.comete.box.adapter;

import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;

import fr.esrf.Tango.DevState;
import fr.esrf.TangoApi.StateUtilities;
import fr.soleil.comete.definition.widget.util.CometeColor;
import fr.soleil.comete.tango.data.adapter.StateAdapter;
import fr.soleil.data.adapter.AbstractAdapter;
import fr.soleil.data.exception.DataAdaptationException;
import fr.soleil.lib.project.ObjectUtils;

public class SimpleStateAdapter extends AbstractAdapter<String, CometeColor> {

    protected final Map<String, CometeColor> colorMap;

    public SimpleStateAdapter() {
        super(String.class, CometeColor.class);
        colorMap = new HashMap<String, CometeColor>();
        colorMap.put(StateUtilities.getNameForState(DevState.ON), StateAdapter.ON);
        colorMap.put(StateUtilities.getNameForState(DevState.OFF), StateAdapter.OFF);
        colorMap.put(StateUtilities.getNameForState(DevState.CLOSE), StateAdapter.CLOSE);
        colorMap.put(StateUtilities.getNameForState(DevState.OPEN), StateAdapter.OPEN);
        colorMap.put(StateUtilities.getNameForState(DevState.INSERT), StateAdapter.INSERT);
        colorMap.put(StateUtilities.getNameForState(DevState.EXTRACT), StateAdapter.EXTRACT);
        colorMap.put(StateUtilities.getNameForState(DevState.MOVING), StateAdapter.MOVING);
        colorMap.put(StateUtilities.getNameForState(DevState.STANDBY), StateAdapter.STANDBY);
        colorMap.put(StateUtilities.getNameForState(DevState.FAULT), StateAdapter.FAULT);
        colorMap.put(StateUtilities.getNameForState(DevState.INIT), StateAdapter.INIT);
        colorMap.put(StateUtilities.getNameForState(DevState.RUNNING), StateAdapter.RUNNING);
        colorMap.put(StateUtilities.getNameForState(DevState.ALARM), StateAdapter.ALARM);
        colorMap.put(StateUtilities.getNameForState(DevState.DISABLE), StateAdapter.DISABLE);
        colorMap.put(StateUtilities.getNameForState(DevState.UNKNOWN), StateAdapter.UNKNOWN);
    }

    @Override
    public CometeColor adapt(String data) throws DataAdaptationException {
        return colorMap.get(data);
    }

    @Override
    public String revertAdapt(CometeColor data) throws DataAdaptationException {
        String result = null;
        for (Entry<String, CometeColor> entry : colorMap.entrySet()) {
            CometeColor color = entry.getValue();
            String key = entry.getKey();
            if (ObjectUtils.sameObject(color, data)) {
                result = key;
                break;
            }
        }
        return result;
    }

}
