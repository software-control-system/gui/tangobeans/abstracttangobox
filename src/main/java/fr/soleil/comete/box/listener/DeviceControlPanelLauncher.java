package fr.soleil.comete.box.listener;

import java.awt.Component;
import java.awt.Container;
import java.awt.Cursor;
import java.awt.Font;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.font.TextAttribute;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.text.JTextComponent;

import fr.soleil.comete.box.AbstractTangoBox;
import fr.soleil.comete.box.scalarbox.StringScalarBox;
import fr.soleil.comete.swing.MatrixPanel;
import fr.soleil.lib.project.SystemUtils;
import fr.soleil.lib.project.file.BatchExecutor;

/**
 * A {@link MouseAdapter} that may listen to {@link JLabel}, {@link JTextComponent} or a {@link MatrixPanel}, in order
 * to change font to notify for the possibility to click on it, and to launch control panel on corresponding device when
 * clicked.
 * 
 * @author GIRARDOT
 *
 */
public class DeviceControlPanelLauncher extends MouseAdapter {

    protected static final String CONTROL_PANEL = "controlPanel";
    protected static final Cursor HAND_CURSOR = new Cursor(Cursor.HAND_CURSOR);

    protected JComponent lastTextComponent;
    protected final Map<TextAttribute, Integer> fontAttributes;
    protected final Font font;
    protected String textToAvoid;

    public DeviceControlPanelLauncher(Font font) {
        super();
        lastTextComponent = null;
        this.font = font;
        fontAttributes = new HashMap<>();
        fontAttributes.put(TextAttribute.UNDERLINE, TextAttribute.UNDERLINE_ON);
        textToAvoid = null;
    }

    protected String recoverDevice(JComponent component) {
        String device = null;
        if (component instanceof JLabel) {
            device = ((JLabel) component).getText();
        } else if (component instanceof JTextComponent) {
            device = ((JTextComponent) component).getText();
        }
        return device;
    }

    /**
     * Recovers the device on which to launch control panel from a {@link MouseEvent}.
     * 
     * @param e The {@link MouseEvent}.
     * @return A {@link String}: the device.
     */
    protected String recoverDevice(MouseEvent e) {
        String device = null;
        if (e != null) {
            Object source = e.getSource();
            if (source instanceof MatrixPanel) {
                device = ((MatrixPanel) source).getStringValueAt(e.getX(), e.getY());
            } else if (source instanceof Container) {
                device = recoverDevice(recoverTextComponent((Container) e.getSource(), e.getX(), e.getY()));
            }
        }
        return device;
    }

    protected boolean isInvalidDeviceName(String device) {
        return (device == null) || device.trim().isEmpty() || device.equals(textToAvoid)
                || AbstractTangoBox.NO_DEVICE_STRING.equalsIgnoreCase(device)
                || StringScalarBox.DEFAULT_ERROR_STRING.equals(device);
    }

    @Override
    public void mouseClicked(MouseEvent e) {
        String device = recoverDevice(e);
        if (!isInvalidDeviceName(device)) {
            new BatchExecutor(SystemUtils.getSystemProperty(CONTROL_PANEL), Arrays.asList(device)).execute();
        }
    }

    @Override
    public void mouseEntered(MouseEvent e) {
        treatSelection(e, true);
    }

    @Override
    public void mouseExited(MouseEvent e) {
        treatSelection(e, false);
    }

    @Override
    public void mouseMoved(MouseEvent e) {
        treatSelection(e, true);
    }

    @Override
    public void mouseDragged(MouseEvent e) {
        treatSelection(e, true);
    }

    /**
     * Recovers the {@link JLabel} or {@link JTextComponent} from a {@link Container} at given position.
     * 
     * @param panel The {@link Container}.
     * @param x The x position.
     * @param y The y position.
     * @return A {@link JComponent}. May be <code>null</code>.
     */
    protected JComponent recoverTextComponent(Container panel, int x, int y) {
        JComponent result;
        Component comp = panel.getComponentAt(x, y);
        if (((comp instanceof JLabel) || (comp instanceof JTextComponent))) {
            JComponent tmp = (JComponent) comp;
            if (isInvalidDeviceName(recoverDevice(tmp))) {
                result = null;
            } else {
                result = tmp;
            }
        } else {
            result = null;
        }
        return result;
    }

    protected synchronized void treatSelection(MouseEvent e, boolean underline) {
        if ((e != null) && (e.getSource() instanceof Container)) {
            Container panel = (Container) e.getSource();
            JComponent component = recoverTextComponent(panel, e.getX(), e.getY());
            if (component != lastTextComponent) {
                Font font = this.font;
                if (font == null) {
                    font = panel.getFont();
                }
                if (lastTextComponent != null) {
                    lastTextComponent.setFont(font);
                }
                lastTextComponent = component;
                if (underline && (component != null)) {
                    component.setFont(font.deriveFont(fontAttributes));
                    panel.setCursor(HAND_CURSOR);
                } else {
                    panel.setCursor(Cursor.getDefaultCursor());
                }
            }
        }
    }

    /**
     * Returns the text to avoid in a {@link JComponent} to consider that component as clickable.
     * 
     * @return The text to avoid. If such a text is found in a {@link JComponent}, that component will be
     *         considered as not clickable.
     */
    public String getTextToAvoid() {
        return textToAvoid;
    }

    /**
     * Sets the text to avoid in a {@link JComponent} to consider that component as clickable.
     * 
     * @param textToAvoid The text to avoid. If such a text is found in a {@link JComponent}, that component will be
     *            considered as not clickable.
     */
    public void setTextToAvoid(String textToAvoid) {
        this.textToAvoid = textToAvoid;
    }

}
