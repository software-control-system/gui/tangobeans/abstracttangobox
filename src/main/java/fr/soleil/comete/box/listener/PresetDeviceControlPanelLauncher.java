package fr.soleil.comete.box.listener;

import java.awt.Container;
import java.awt.Font;
import java.awt.event.MouseEvent;

import javax.swing.JComponent;

import fr.soleil.comete.swing.MatrixPanel;

/**
 * A {@link DeviceControlPanelLauncher} that will launch control panel on previously set device.
 * 
 * @author GIRARDOT
 */
public class PresetDeviceControlPanelLauncher extends DeviceControlPanelLauncher {

    protected String device;

    public PresetDeviceControlPanelLauncher(Font font) {
        super(font);
    }

    @Override
    protected String recoverDevice(JComponent component) {
        return this.device;
    }

    @Override
    protected String recoverDevice(MouseEvent e) {
        String device = null;
        if (e != null) {
            Object source = e.getSource();
            if (source instanceof MatrixPanel) {
                device = this.device;
            } else if (source instanceof Container) {
                JComponent component = recoverTextComponent((Container) e.getSource(), e.getX(), e.getY());
                if (component != null) {
                    device = this.device;
                }
            }
        }
        return device;
    }

    /**
     * Returns the device on which to launch the control panel.
     * 
     * @return A {@link String}.
     */
    public String getDevice() {
        return device;
    }

    /**
     * Sets the device on which to launch the control panel.
     * 
     * @param device The device on which to launch the control panel.
     */
    public void setDevice(String device) {
        this.device = device;
    }

}
