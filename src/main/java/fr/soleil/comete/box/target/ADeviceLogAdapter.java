package fr.soleil.comete.box.target;

import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import fr.soleil.data.mediator.Mediator;
import fr.soleil.data.target.matrix.ITextMatrixTarget;
import fr.soleil.lib.project.ObjectUtils;
import fr.soleil.lib.project.application.logging.LogViewer;
import fr.soleil.lib.project.log.Level;
import fr.soleil.lib.project.log.LogData;

/**
 * Common class used to receive logs from tango devices and transmit them to a {@link LogViewer}.
 *
 * @param <T> The type of data this {@link ADeviceLogAdapter} is able to parse to recover {@link LogData}.
 * 
 * @author GIRARDOT
 */
public abstract class ADeviceLogAdapter<T> implements ITextMatrixTarget {

    private final WeakReference<LogViewer> logViewerRef;
    protected final Map<String, Level> customLevels;

    /**
     * Constructs a new {@link ADeviceLogAdapter}.
     * 
     * @param logViewer The associated {@link LogViewer}.
     */
    public ADeviceLogAdapter(LogViewer logViewer) {
        this.logViewerRef = logViewer == null ? null : new WeakReference<>(logViewer);
        customLevels = new ConcurrentHashMap<>();
    }

    /**
     * Registers a custom level name, so that this {@link ADeviceLogAdapter} will be able to parse that name and recover
     * matching {@link Level}.
     * 
     * @param name The custom level name.
     * @param level The matching {@link Level}.
     */
    public void registerCustomLevelName(String name, Level level) {
        if ((name != null) && (level != null)) {
            customLevels.put(name.trim().toLowerCase(), level);
        }
    }

    /**
     * Forgets a custom level name. This {@link ADeviceLogAdapter} won't be able to parse that name in order to recover
     * matching {@link Level}.
     * 
     * @param name The custom level name.
     */
    public void forgetCustomLevelName(String name) {
        if (name != null) {
            customLevels.remove(name.trim().toLowerCase());
        }
    }

    /**
     * Recovers a {@link Level} from a name.
     * 
     * @param name The name.
     * @return A {@link Level}.
     */
    protected Level parseLevel(String name) {
        Level level = Level.parseLevel(name);
        if ((level == null) && (name != null)) {
            level = customLevels.get(name.trim().toLowerCase());
        }
        return level;
    }

    /**
     * Returns the {@link LogViewer} associated with this {@link ADeviceLogAdapter}.
     * 
     * @return A {@link LogViewer}.
     */
    protected LogViewer getLogViewer() {
        return ObjectUtils.recoverObject(logViewerRef);
    }

    /**
     * Parses some data to recover some {@link LogData}.
     * 
     * @param data The data to parse;
     * @return A {@link LogData}.
     */
    protected abstract LogData parseLog(T data);

    /**
     * Treats a data array to transmit corresponding logs to {@link LogViewer}.
     * 
     * @param dataArray The data array.
     */
    protected void treatData(T[] dataArray) {
        LogViewer logViewer = getLogViewer();
        if (logViewer != null) {
            if ((dataArray != null) && (dataArray.length > 0)) {
                List<LogData> logList = new ArrayList<>(dataArray.length);
                for (T data : dataArray) {
                    LogData log = parseLog(data);
                    if (log != null) {
                        logList.add(log);
                    }
                }
                if (logList.isEmpty()) {
                    logViewer.clearLogs();
                } else {
                    LogData[] logs = logList.toArray(new LogData[logList.size()]);
                    logList.clear();
                    boolean keepLast = logs[0].getTimestamp().compareTo(logs[logs.length - 1].getTimestamp()) < 0;
                    logViewer.setLogs(keepLast, logs);
                }
            } else {
                logViewer.clearLogs();
            }
        }
    }

    @Override
    public int getMatrixDataWidth(Class<?> concernedDataClass) {
        // not managed
        return 0;
    }

    @Override
    public int getMatrixDataHeight(Class<?> concernedDataClass) {
        // not managed
        return 0;
    }

    @Override
    public void addMediator(Mediator<?> mediator) {
        // not managed
    }

    @Override
    public void removeMediator(Mediator<?> mediator) {
        // not managed
    }

    @Override
    public String[][] getStringMatrix() {
        // not managed
        return null;
    }

    @Override
    public String[] getFlatStringMatrix() {
        // not managed
        return null;
    }

    @Override
    protected void finalize() throws Throwable {
        customLevels.clear();
        super.finalize();
    }
}
