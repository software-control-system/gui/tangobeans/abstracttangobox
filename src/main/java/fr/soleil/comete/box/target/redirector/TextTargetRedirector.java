package fr.soleil.comete.box.target.redirector;

import fr.soleil.data.target.scalar.ITextTarget;

/**
 * This class is used to immediately redirect
 * 
 * @author huriez
 * 
 */
public abstract class TextTargetRedirector extends AbstractTargetRedirector<String> implements
ITextTarget {

    @Override
    public String getText() {
        return lastValue;
    }

    @Override
    public void setText(String text) {
        lastValue = text;
        methodToRedirect(text);
    }
}
