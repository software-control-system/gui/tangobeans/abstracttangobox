package fr.soleil.comete.box.target.redirector;

import fr.soleil.data.mediator.Mediator;
import fr.soleil.data.source.AbstractDataSource;
import fr.soleil.data.target.TargetDelegate;
import fr.soleil.data.target.information.TargetInformation;

public abstract class AbstractTargetRedirector<U> implements ITargetRedirector<U> {

    private U parameter;
    private final TargetDelegate targetDelegate = new TargetDelegate();

    // memorize the current value
    protected U lastValue;

    /**
     * @param parameter the parameter to set
     */
    public void setParameter(U parameter) {
        this.parameter = parameter;
    }

    /**
     * @return the parameter
     */
    public U getParameter() {
        return parameter;
    }

    @Override
    public void addMediator(Mediator<?> mediator) {
        targetDelegate.addMediator(mediator);
    }

    @Override
    public void removeMediator(Mediator<?> mediator) {
        targetDelegate.removeMediator(mediator);
    }

    /**
     * Send the parameter registered by the object to the linked {@link AbstractDataSource} in order
     * to ask for refreshment.
     */
    public void execute() {
        targetDelegate.warnMediators(new TargetInformation<U>(this, parameter));
    }
}
