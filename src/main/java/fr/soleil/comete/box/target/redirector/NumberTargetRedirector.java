package fr.soleil.comete.box.target.redirector;

import fr.soleil.data.target.scalar.INumberTarget;

public abstract class NumberTargetRedirector extends AbstractTargetRedirector<Number> implements INumberTarget {

    @Override
    public Number getNumberValue() {
        return lastValue;
    }

    @Override
    public void setNumberValue(Number value) {
        lastValue = value;
        methodToRedirect(value);
    }

}
