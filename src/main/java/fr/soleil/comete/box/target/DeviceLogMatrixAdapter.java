package fr.soleil.comete.box.target;

import fr.soleil.lib.project.application.logging.LogViewer;
import fr.soleil.lib.project.log.LogData;
import fr.soleil.lib.project.math.ArrayUtils;

/**
 * An {@link ADeviceLogAdapter} dedicated to String matrix device logs.
 * 
 * @author GIRARDOT
 */
public class DeviceLogMatrixAdapter extends ADeviceLogAdapter<String[]> {

    public DeviceLogMatrixAdapter(LogViewer logViewer) {
        super(logViewer);
    }

    @Override
    public boolean isPreferFlatValues(Class<?> concernedDataClass) {
        return false;
    }

    @Override
    protected LogData parseLog(String[] data) {
        LogData log;
        if ((data != null) && (data.length > 2) && (data[0] != null) && (data[1] != null) && (data[2] != null)) {
            log = new LogData(data[0], parseLevel(data[1]), data[2]);
        } else {
            log = null;
        }
        return log;
    }

    @Override
    public void setStringMatrix(String[][] value) {
        treatData(value);
    }

    @Override
    public void setFlatStringMatrix(String[] value, int width, int height) {
        setStringMatrix((String[][]) ArrayUtils.convertArrayDimensionFrom1ToN(value, height, width));
    }

}
