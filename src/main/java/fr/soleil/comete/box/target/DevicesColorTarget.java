package fr.soleil.comete.box.target;

import java.lang.reflect.Array;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fr.soleil.comete.box.adapter.SimpleStateAdapter;
import fr.soleil.comete.definition.widget.util.CometeColor;
import fr.soleil.comete.swing.MatrixPanel;
import fr.soleil.data.container.matrix.StringMatrix;
import fr.soleil.data.exception.DataAdaptationException;
import fr.soleil.data.mediator.Mediator;
import fr.soleil.data.target.TargetDelegate;
import fr.soleil.data.target.matrix.ITextMatrixTarget;

public class DevicesColorTarget implements ITextMatrixTarget {

    protected final StringMatrix matrix;
    protected final TargetDelegate delegate;
    protected final SimpleStateAdapter stateAdapter;
    private final MatrixPanel matrixPanel;
    protected final Logger logger;

    public DevicesColorTarget(MatrixPanel pMatrixPanel, Logger logger) {
        matrix = new StringMatrix();
        delegate = new TargetDelegate();
        stateAdapter = new SimpleStateAdapter();
        matrixPanel = pMatrixPanel;
        this.logger = logger == null ? LoggerFactory.getLogger(Mediator.LOGGER_ACCESS) : logger;
    }

    @Override
    public String[] getFlatStringMatrix() {
        return matrix.getFlatValue();
    }

    @Override
    public String[][] getStringMatrix() {
        return matrix.getValue();
    }

    @Override
    public void setFlatStringMatrix(String[] value, int width, int height) {
        try {
            CometeColor[] cometeColors = new CometeColor[value.length];
            for (int i = 0; i < value.length; i++) {
                cometeColors[i] = stateAdapter.adapt(value[i]);
            }
            matrixPanel.setFlatCometeColorMatrix(cometeColors, width, height);
        } catch (DataAdaptationException e) {
            logger.error(e.getMessage(), e);
        }
        matrix.setFlatValue(value, width, height);
    }

    @Override
    public void setStringMatrix(String[][] value) {
        CometeColor[][] cometeColors = null;
        if (value != null) {
            try {
                cometeColors = new CometeColor[value.length][];
                for (int i = 0; i < value.length; i++) {
                    cometeColors[i] = new CometeColor[value[i].length];
                    for (int j = 0; j < value[i].length; j++) {
                        cometeColors[i][j] = stateAdapter.adapt(value[i][j]);
                    }
                }
            } catch (DataAdaptationException e) {
                logger.error(e.getMessage(), e);
            }
        }
        matrix.setValue(value);
        matrixPanel.setCometeColorMatrix(cometeColors);
    }

    @Override
    public int getMatrixDataHeight(Class<?> concernedDataClass) {
        int height = 0;
        if (matrix.getType().isAssignableFrom(concernedDataClass)) {
            Object[] value = matrix.getValue();
            if (value != null) {
                height = value.length;
            }
        }
        return height;
    }

    @Override
    public int getMatrixDataWidth(Class<?> concernedDataClass) {
        int width = 0;
        if (matrix.getType().isAssignableFrom(concernedDataClass)) {
            Object[] value = matrix.getValue();
            if ((value != null) && (value.length > 0)) {
                width = Array.getLength(value[0]);
            }
        }
        return width;
    }

    @Override
    public boolean isPreferFlatValues(Class<?> concernedDataClass) {
        return true;
    }

    @Override
    public void addMediator(Mediator<?> mediator) {
        delegate.addMediator(mediator);
    }

    @Override
    public void removeMediator(Mediator<?> mediator) {
        delegate.removeMediator(mediator);

    }

}