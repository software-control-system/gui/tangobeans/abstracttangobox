package fr.soleil.comete.box.target.redirector;

import fr.soleil.data.target.ITarget;

public interface ITargetRedirector<U> extends ITarget {

    public void methodToRedirect(U data);
}
