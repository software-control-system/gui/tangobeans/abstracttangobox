package fr.soleil.comete.box.target;

import fr.soleil.lib.project.application.logging.LogViewer;
import fr.soleil.lib.project.log.LogData;
import fr.soleil.lib.project.math.ArrayUtils;

/**
 * An {@link ADeviceLogAdapter} dedicated to String spectrum device logs.
 * 
 * @author GIRARDOT
 */
public class DeviceLogSpectrumAdapter extends ADeviceLogAdapter<String> {

    public DeviceLogSpectrumAdapter(LogViewer logViewer) {
        super(logViewer);
    }

    @Override
    public boolean isPreferFlatValues(Class<?> concernedDataClass) {
        return String.class.equals(concernedDataClass);
    }

    @Override
    public void setStringMatrix(String[][] value) {
        int[] shape = ArrayUtils.recoverShape(value);
        int width, height;
        if ((shape == null) || (shape.length == 0)) {
            width = 0;
            height = 0;
        } else if (shape.length == 1) {
            width = shape[0];
            height = 1;
        } else {
            width = shape[0];
            height = shape[1];
        }
        setFlatStringMatrix((String[]) ArrayUtils.convertArrayDimensionFromNTo1(value), width, height);
    }

    /**
     * Recovers a {@link LogData} from a {@link String}.
     * 
     * @param value The {@link String}.
     * @return A {@link LogData};
     */
    @Override
    protected LogData parseLog(String value) {
        LogData log;
        if ((value == null) || (value.isEmpty())) {
            log = null;
        } else {
            int firstIndex = value.indexOf('[');
            if (firstIndex > -1) {
                int secondIndex = value.indexOf(']', firstIndex);
                if (secondIndex > -1) {
                    log = new LogData(value.substring(0, firstIndex).trim(),
                            parseLevel(value.substring(firstIndex + 1, secondIndex)),
                            value.substring(secondIndex + 1).trim());
                } else {
                    log = null;
                }
            } else {
                log = null;
            }
        }
        return log;
    }

    @Override
    public void setFlatStringMatrix(String[] value, int width, int height) {
        treatData(value);
    }

}
