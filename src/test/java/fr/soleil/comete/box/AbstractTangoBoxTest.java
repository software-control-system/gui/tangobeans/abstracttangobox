package fr.soleil.comete.box;

import java.awt.BorderLayout;
import java.util.prefs.Preferences;

import javax.swing.JFrame;
import javax.swing.SwingUtilities;
import javax.swing.WindowConstants;

import fr.soleil.comete.tango.data.service.helper.TangoDeviceHelper;

public class AbstractTangoBoxTest extends AbstractTangoBox {

    private static final long serialVersionUID = 1601862823384127679L;

    public AbstractTangoBoxTest() {
        super();
        initDisplayedComponents();
    }

    /*
     * Initializes the displayed components/widgets
     */
    private void initDisplayedComponents() {
        // getStatusPanel().setBorder(new LineBorder(Color.GRAY));
        setLayout(new BorderLayout());
        add(getStateLabel(), BorderLayout.NORTH);
        add(getStatusPanel(), BorderLayout.CENTER);
    }

    @Override
    protected void refreshGUI() {
        if (getModel() != null && !getModel().isEmpty()) {
            setStatusModel();
            System.out.println(TangoDeviceHelper.getDeviceProxy(model));// illustrates Mantis #22817
        }

    }

    @Override
    protected void clearGUI() {
        // cleanStatusModel();
        cleanWidget(getStateLabel());
        cleanWidget(getDeviceStatusLabel());
    }

    @Override
    protected void onConnectionError() {
    }

    @Override
    protected void loadPreferences(Preferences preferences) {
        // nothing to do
    }

    @Override
    protected void savePreferences(Preferences preferences) {
        // nothing to do
    }

    public static void main(String[] args) {
        final JFrame mainFrame = new JFrame();
        mainFrame.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        AbstractTangoBoxTest bean = new AbstractTangoBoxTest();
        mainFrame.setContentPane(bean);
        mainFrame.setTitle(bean.getClass().getSimpleName());
        if (args != null && args.length > 0) {
            String model = args[0];
            startBean(bean, model);
            stopBean(bean);
            startBean(bean, model);
            // startBean(bean, model);
            // stopBean(bean);
            // startBean(bean, model);
        }
        SwingUtilities.invokeLater(new Runnable() {
            @Override
            public void run() {
                mainFrame.pack();
                mainFrame.setLocationRelativeTo(null);
                mainFrame.setVisible(true);
            }
        });
    }

    private static void startBean(AbstractTangoBoxTest bean, String model) {
        System.out.println("START MODEL IN =" + System.currentTimeMillis());
        bean.setModel(model);
        bean.start();
        System.out.println("START MODEL OUT =" + System.currentTimeMillis());
    }

    private static void stopBean(AbstractTangoBoxTest bean) {
        System.out.println("STOP MODEL IN =" + System.currentTimeMillis());
        bean.stop();
        bean.setModel(null);
        System.out.println("STOP MODEL OUT =" + System.currentTimeMillis());
    }

}
